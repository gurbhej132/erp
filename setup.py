# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in client_portal/__init__.py
from client_portal import __version__ as version

setup(
	name='client_portal',
	version=version,
	description='Client Portal',
	author='Client Portal',
	author_email='navdeepghai1@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
