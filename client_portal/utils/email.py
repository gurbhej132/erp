'''
'''

import frappe
from frappe import _
from frappe.utils import today, nowdate, now_datetime, add_days, get_formatted_email


STANDARD_USERS = ("Guest", "Administrator")


def update_email(email_id, logout_all_sessions=0, key=None, now=True):
    from frappe.utils import random_string, get_url
    from frappe.utils.user import get_user_fullname

    if(frappe.session.user in ["administrator", "Administrator"]):
        frappe.throw(_("Can't request to change administrator id"))

    if(email_id == frappe.session.user):
        frappe.throw(("New email id can't  be same as existing email id"))

    if(frappe.db.get_value("User", {"email": email_id}) or
        frappe.db.get_value("User", email_id)):
        frappe.throw(_("Email id %s is already used by other account"%(email_id)))

    now_date = now_datetime()
    doc = frappe.get_doc({
        "doctype": "Email Update Request",
        "last_email": frappe.session.user,
        "new_email": email_id,
        "datetime_request": now_date,
        "validity": add_days(now_date, 1),
        "email_key": random_string(32),
        "status": "Pending",
        "ip_address": frappe.local.request_ip
        })

    doc.save(ignore_permissions=True)

    validity = add_days(now_date, 1)

    link = get_url("/update-email?key=" + doc.email_key)
    disable_user_link = get_url("/update-email?key=%s&disabled=%s"%(doc.email_key, True))

    # Disabled user, if user didn't raise request to reset the password.

    user = frappe.get_doc("User", frappe.session.user)
    args = {
        "doc": doc, "user": user, "link": link,
        "disable_user_link": disable_user_link
    }
    frappe.db.commit()

    sender = frappe.session.user not in STANDARD_USERS and get_formatted_email(frappe.session.user) or None

    frappe.sendmail(recipients=email_id, sender=sender, subject="Email Changed Request",
    	template="new_email", args=args, header=["Email Changed Request", "green"],
    	delayed=(not now) if now!=None else self.flags.delay_emails, retry=3)

    return {"title": _("Confirmation has sent your email")}
