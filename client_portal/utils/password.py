'''
'''

import frappe
import re

def test_password_strength(password, confirmpassword=False):

	reg = "^(?:(?=.*?[A-Z])(?:(?=.*?[0-9])(?=.*?[-!@#$%^&*()_[\]{},.<>+=])|(?=.*?[a-z])(?:(?=.*?[0-9])|(?=.*?[-!@#$%^&*()_[\]{},.<>+=])))|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[-!@#$%^&*()_[\]{},.<>+=]))[A-Za-z0-9!@#$%^&*()_[\]{},.<>+=-]{8,20}$"
	pat = re.compile(reg)

	flag = re.search(pat, password)
	if(flag):
		flag = True
	else:
		flag = False

	if(confirmpassword and not(confirmpassword == password)):
		flag = False
		msg = "<b>The password and confirm password doesn't match</b>"
	else:
		msg = """<ul><b>The password must contain at least three character categories among the following:</b>
					<li>Uppercase characters (A-Z)</li>
					<li>Lowercase characters (a-z)</li>
					<li>Digits (0-9)</li>
					<li>Special characters (~!@#$%^&*_-+=`|\(){}[]:;"'<>,.?/)</li><ul>"""

	return {"flag": flag, "error_message": msg}
