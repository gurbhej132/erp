'''
    Manage permissions here
'''
import frappe
from frappe import _
from frappe.utils import flt

# standard libs for permission manager
from frappe.permissions import add_permission, update_permission_property

def setup_permissions(user):
    setup_user_permissions(user)
    setup_business_permissions(user)
    setup_address_permissions(user)

    update_permission_property(doctype, role, permlevel, ptype, value)


def add_permission(user, doctype, value, apply_to_all_doctypes=False):

    if(not frappe.db.get_value("User Permission", {
            "allow": doctype, "user": user, "value": value
            })):
        return False

    permission = frappe.get_doc({
        "doctype": "User Permission", "for_value": value,
        "user": user, "allow": doctype,
        "apply_to_all_doctypes": apply_to_all_doctypes
    })

    try:
        permission.save(ignore_permissions=True)
    except Exception as e:
        print(frappe.get_traceback())


PERMISSIONS_ATTR = [
    'delete', 'write', 'create', 'delete', 'print',
    'email', 'report', 'import', 'export', 'set_user_permissions',
    'share', 'if_owner'
]

ADD_PERMISSIONS_ATTR = [
    'doctype', 'permlevel', 'role', 'value', 'ptype'
]

'''
    VALUE CAN BE 1 OR 0
    DOCTYPE:['STANDARD TABLES']
    PERMLEVEL: 0,1,2...9
    ROLE: ['STANDARD ROLES']
    PTYPE(PERMISSIONS TYPE): [PERMISSIONS_ATTR]
'''
