'''
'''

import frappe
import importlib
import os
import json
from six import string_types

APP_NAME = "client_portal"
BASE_FOLDER = "www"

'''
    Get Form meta
'''
@frappe.whitelist()
def get_form_meta(route, method=None):
    if not method:
        method = "get_form"
    controller = get_controller(route, method)
    forms = []
    if controller:
        forms = controller(route, method)
    return forms

'''
    Save form changes in data base
'''
@frappe.whitelist()
def save_form(form, route, method=None):
    if not(method):
        method = "save_form"

    if(form and isinstance(form, string_types)):
        form = frappe._dict(json.loads(form))

    controller = get_controller(route, method)
    return controller(form, route, method)

'''
    Get Basic controller
'''
def get_controller(route, method):
    global APP_NAME, BASE_FOLDER
    APP_PATH = "%s%s"%(os.path.join(frappe.get_app_path("client_portal"), BASE_FOLDER), route)
    forms = []
    if(not os.path.exists(APP_PATH) or not route):
        return form

    if(os.path.isdir(APP_PATH)):
        route = "%s/%s"%(route, "index")

    route = route.replace("/", ".")

    try:
        path = "%s.%s%s"%(APP_NAME, BASE_FOLDER, route)
        _module = importlib.import_module(path)

        # Return the controller function
        return getattr(_module, method, None)
    except  ImportError as e:
        print(frappe.get_traceback())
        raise e


'''
    Get existing address
'''
@frappe.whitelist()
def get_address(address=None):
    if(not address or not frappe.db.get_value("Address", address)):
        return {}
    return frappe.get_doc("Address", address).as_dict()
