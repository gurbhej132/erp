
// mobile menu
let hamburger = $('#hamburger-icon'),
    showMenu = $('#js-navbar'),
    overlay = $('html');
//mob menu
hamburger.click(function () {
    hamburger.toggleClass('active');
    showMenu.toggleClass('show');
    overlay.toggleClass('overflow-hidden');
    return false;
});

/*slider form start*/
if (window.innerWidth < 768) {
    $('.form-slider').slick({
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: false,
        // adaptiveHeight: true,
        infinite: false
    });
}

// SLIDERS
$(window).on('load resize', function () {
    if (window.innerWidth < 768) {
        $('.form-slider').not('.slick-initialized').slick({
            arrows: false,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: false,
            infinite: false
        });
        $('.icon-slider').not('.slick-initialized').slick({
            arrows: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 2,
            variableWidth: false,
            infinite: false,
            responsive: [
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: false
                    }
                }
            ]
        });
    } else {
        $('.form-slider.slick-initialized').slick('unslick');
        $('.icon-slider.slick-initialized').slick('unslick');
    }
});

// Custom Select
$('.nice-select-custom').niceSelect();

$('.nice-select-custom').on('change', function () {
    $(this).parent().find('.nice-select-custom').removeClass('text-muted').addClass('select-active');
});

$('[data-toggle="tooltip"]').tooltip('enable');
$('#js-vin-tooltip').tooltip('enable');

$(window).on('load resize', function () {
    if ($(window).width() > 991) {
        $('#js-tooltip-id').tooltip('enable');
        $('#js-tooltip-apply').tooltip('enable');
        $('#js-tooltip-services').tooltip('enable');
        $('#js-tooltip-help').tooltip('enable');
        $('#js-tooltip-account').tooltip('enable');
        $('#js-tooltip-notice').tooltip('enable');
        // $('#js-support-btn').tooltip('enable');
    }
    else {
        $('#js-tooltip-id').tooltip('disable');
        $('#js-tooltip-apply').tooltip('disable');
        $('#js-tooltip-services').tooltip('disable');
        $('#js-tooltip-help').tooltip('disable');
        $('#js-tooltip-account').tooltip('disable');
        $('#js-tooltip-notice').tooltip('disable');
        $('#js-support-btn').tooltip('disable');
    }
});

$('[data-toggle="popover"]').popover();

$('#js-check-diff-address').click(function () {
    if ($(this).is(':checked')) {
        $('#js-diff-address').removeClass('d-none');
    } else {
        $('#js-diff-address').addClass('d-none');
    }
});

// display remove member button on form
$('.js-display-remove').mouseover(function () {
    $(this).find('.btn-delete').removeClass('d-none');
    $(this).find('.input-group-append').addClass('d-none');
});
$('.js-display-remove').mouseout(function () {
    $(this).find('.btn-delete').addClass('d-none');
    $(this).find('.input-group-append').removeClass('d-none');
});

//FORM NAVIGATION
// disable submit on Enter
$('#msform').bind("keypress", function(e) {
    if (e.keyCode == 13) {
        return false;
    }
});

let current_fs, previous_fs, next_fs; //fieldsets
// buttons prev
$('.btn-prev').click(function () {
    current_fs = $(this).parent().parent().parent().parent().parent().parent();
    previous_fs = $(this).parent().parent().parent().parent().parent().parent().prev();
    current_fs.hide();
    previous_fs.fadeIn(1000);
    $('html, body').animate({scrollTop: 0}, 500);
    return false;
});
// buttons back
$('.back').click(function () {
    current_fs = $(this).parent().parent().parent().parent();
    previous_fs = $(this).parent().parent().parent().parent().prev();
    current_fs.hide();
    previous_fs.fadeIn(1000);
    $('html, body').animate({scrollTop: 0}, 500);
    return false;
});
// buttons next
$('.next').click(function () {
    current_fs = $(this).parent().parent().parent().parent();
    next_fs = $(this).parent().parent().parent().parent().next();
    current_fs.hide();
    next_fs.fadeIn(1000);
    $('html, body').animate({scrollTop: 0}, 500);
    return false;
});
// buttons submit
$('.submit-home').click(function () {
    $('#progress-wrap').fadeOut(500);
    $('.main-form-landing').fadeTo(500, 0);
    $('#js-form-finish').slideDown(1300);
    $('#js-animation-opacity').addClass('opacity-fade-in');
    $('#js-content-box-opacity').addClass('opacity-fade-in-text');
    $('html').toggleClass('overflow-hidden');
    return false;
});

$('.submit-car').click(function () {
    $('#progress-wrap').fadeOut(500);
    $('.main-form-landing').fadeTo(500, 0);
    $('#js-form-finish').slideDown(1300);
    $('#js-animation-opacity').addClass('opacity-fade-in');
    $('#js-content-box-opacity').addClass('opacity-fade-in-text');
    $('html').toggleClass('overflow-hidden');
    return false;
});

//decore text input
$('.js-text-form').change(function () {
    if ($(this).val()) {
        $(this).parent().addClass('not-empty');
    }
    if ($(this).val() === '') {
        $(this).parent().removeClass('not-empty');
    }
});

//text input scroll
$('.js-next-field-input').change(function () {
    let icon = $(this).parent().parent().parent().parent().find('.check-icons'),
        nextField = $(this).parent().parent().parent().parent().next('.js-form-field');
    if ($(this).val()) {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextField).find('.form-disabled:first').css('height', '0');
        $('html, body').animate({
            scrollTop: nextField.offset().top - 200
        }, 500);
    }
    if ($(this).val() === '') {
        $(icon).find('.active').addClass('d-none');
        $(icon).find('.inactive').removeClass('d-none');
        $(nextField).find('.form-disabled:first').css('height', '100%');
    }
});

$('.js-next-field-input').bind('keypress', function(e) {
    let icon = $(this).parent().parent().parent().parent().find('.check-icons'),
        nextField = $(this).parent().parent().parent().parent().next('.js-form-field');
    if (e.keyCode == 13 && $(this).val()) {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextField).find('.form-disabled:first').css('height', '0');
        $('html, body').animate({
            scrollTop: nextField.offset().top - 200
        }, 500);
    }
});

$('.js-text-not-empty-tab').bind('keypress', function(e) {
    let nextTextForm = $(this).parent().parent().next().find('.js-text-form');
    if (e.keyCode == 13 && $(this).val()) {
        $(nextTextForm).focus();
    }
});

$('.js-field-input-only-icon').change(function() {
    let icon = $(this).parent().parent().parent().parent().find('.check-icons');
        if ($(this).val()) {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
    }
    if ($(this).val() === '') {
        $(icon).find('.active').addClass('d-none');
        $(icon).find('.inactive').removeClass('d-none');
    }
});


//radio input scroll
$('input[type=radio].js-next-field-radio').on('change', function (e) {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextField = $(this).parent().parent().parent().parent().parent().next('.js-form-field');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');
    $(nextField).find('.form-disabled').css('height', '0');
    $('html, body').animate({
        scrollTop: nextField.offset().top - 200
    }, 500);
});

//hide or clone radio
$('input[type=radio].js-next-field-radio-clone').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextFieldHide = $(this).parent().parent().parent().parent().parent().next('.js-form-field-hide'),
        nextField = $(this).parent().parent().parent().parent().parent().next().next('.js-form-field-after-hide');
    if (this.value === 'Yes') {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).removeClass('d-none');
        $(nextField).find('.form-disabled').css('height', '0');
        $('html, body').animate({
            scrollTop: nextFieldHide.offset().top - 200
        }, 500);
    }
    else if (this.value === 'No') {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).addClass('d-none');
        $(nextField).find('.form-disabled').css('height', '0');
        $('html, body').animate({
            scrollTop: nextField.offset().top - 200
        }, 500);
    }
    else {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).removeClass('d-none');
        $('html, body').animate({
            scrollTop: nextFieldHide.offset().top - 200
        }, 500);
    }
});

//select input scroll
$('select.js-next-field-select').on('change', function () {
    let icon = $(this).parent().parent().parent().find('.check-icons'),
        nextField = $(this).parent().parent().parent().next('.js-form-field');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');
    $(nextField).find('.form-disabled').css('height', '0');
    $('html, body').animate({
        scrollTop: nextField.offset().top - 200
    }, 500);
});

//checkbox input scroll
$('input[type=checkbox].js-next-field-checkbox').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextField = $(this).parent().parent().parent().parent().parent().next('.js-form-field'),
        total_checked_boxes = $(this).parent().parent().parent().find('input:checkbox:checked').length;
    if (total_checked_boxes === 0) {
        $(icon).find('.active').addClass('d-none');
        $(icon).find('.inactive').removeClass('d-none');
        $(nextField).find('.form-disabled').css('height', '100%');
    }
    else {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextField).find('.form-disabled').css('height', '0');
    }
});

$('.minus-btn').click(function () {
    let $input = $(this).parent().parent().find('input');
    let count = parseInt($input.val()) - 1;
    count = count < 0 ? 0 : count;
    $input.val(count);
    $input.change();
    return false;
});

$('.plus-btn').click(function () {
    let $input = $(this).parent().parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
});

//select on see-quotes.html
$('select.js-next-field-quotes').on('change', function () {
    let nextField = $(this).parent().parent().parent().next('.js-form-field');
    $(nextField).find('.form-disabled').css('height', '0');
});
