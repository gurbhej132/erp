// CAR FORM JS

//homeowners-path
$('input[type=radio][name=why-shopping-today]').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        pathIcon = $('#path-img'),
        allPathText = $('.path-text'),
        pathText1 = $('#path-text-1'),
        pathText2 = $('#path-text-2'),
        pathText3 = $('#path-text-3'),
        pathText4 = $('#path-text-4'),
        pathText5 = $('#path-text-5');

    $(pathIcon).removeClass('d-none');
    $('#submit-home-0').find('.form-disabled').css('height', '0');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');

    if (this.value === '1') {
        $(allPathText).addClass('d-none');
        $(pathText1).removeClass('d-none');
    }
    if (this.value === '2') {
        $(allPathText).addClass('d-none');
        $(pathText2).removeClass('d-none');
    }
    if (this.value === '3') {
        $(allPathText).addClass('d-none');
        $(pathText3).removeClass('d-none');
    }
    if (this.value === '4') {
        $(allPathText).addClass('d-none');
        $(pathText4).removeClass('d-none');
    }
    if (this.value === '5') {
        $(allPathText).addClass('d-none');
        $(pathText5).removeClass('d-none');
    }
});

$('#submit-home-0 .next').click(function () {
    let pathRadio = $('input[type=radio][name=why-shopping-today]'),
        sidebar1 = $('#path-sidebar-1'),
        sidebar2 = $('#path-sidebar-2'),
        sidebar3 = $('#path-sidebar-3'),
        sidebar4 = $('#path-sidebar-4'),
        sidebar5 = $('#path-sidebar-5');
    $('#progress-wrap').removeClass('d-none');
    $('.main-form-landing').removeClass('first-step-padding');
    if(pathRadio[0].checked){
        $(sidebar1).removeClass('d-none');
        $('#home-form-47').remove();
        $('#home-insurance-expired').remove();
        $('#home-insurance-yes').remove();
    }
    if(pathRadio[1].checked){
        $(sidebar2).removeClass('d-none');
    }
    if(pathRadio[2].checked){
        $(sidebar3).removeClass('d-none');
        $('#home-form-2').remove();
        $('#home-form-3').remove();
        $('#home-form-3-1').removeClass('d-none');
        $('#home-form-45').remove();
    }
    if(pathRadio[3].checked){
        $(sidebar4).removeClass('d-none');
    }
    if(pathRadio[4].checked){
        $(sidebar5).removeClass('d-none');
        $('#home-form-2').remove();
        $('#home-form-45').remove();
        $('#home-form-47').remove();
        $('#home-insurance-expired').remove();
        $('#home-insurance-yes').removeClass('d-none');
    }
});


// progress bar
$('#submit-home-1 .next').click(function () {
    $('#progress-home-step-1 .progress-bar').css('width', '100%');
    $('#progress-home-step-2 .progress-bar').css('width', '10%');
    $('#progress-home-step-2 button').removeAttr('disabled');
});

$('#submit-home-2-1 .next-no-fade').click(function () {
    let current_fs, next_fs, next_sub_step; //fieldsets
    current_fs = $(this).parent().parent().parent().parent();
    next_fs = $(this).parent().parent().parent().parent().next();
    next_sub_step = $(this).parent().parent().parent().parent().next().find('.right-col-form');
    current_fs.hide();
    next_fs.fadeIn(0);
    next_sub_step.hide();
    next_sub_step.fadeIn(500);
    $('html, body').animate({scrollTop: 0}, 500);
    $('#progress-home-step-2 .progress-bar').css('width', '25%');
    return false;
});

$('#submit-home-2-2 .next-no-fade').click(function () {
    let current_fs, next_fs, next_sub_step; //fieldsets
    current_fs = $(this).parent().parent().parent().parent();
    next_fs = $(this).parent().parent().parent().parent().next();
    next_sub_step = $(this).parent().parent().parent().parent().next().find('.right-col-form');
    current_fs.hide();
    next_fs.fadeIn(0);
    next_sub_step.hide();
    next_sub_step.fadeIn(500);
    $('html, body').animate({scrollTop: 0}, 500);
    $('#progress-home-step-2 .progress-bar').css('width', '40%');
    return false;
});

$('#submit-home-2-3 .next-no-fade').click(function () {
    let current_fs, next_fs, next_sub_step; //fieldsets
    current_fs = $(this).parent().parent().parent().parent();
    next_fs = $(this).parent().parent().parent().parent().next();
    next_sub_step = $(this).parent().parent().parent().parent().next().find('.right-col-form');
    current_fs.hide();
    next_fs.fadeIn(0);
    next_sub_step.hide();
    next_sub_step.fadeIn(500);
    $('html, body').animate({scrollTop: 0}, 500);
    $('#progress-home-step-2 .progress-bar').css('width', '55%');
    return false;
});

$('#submit-home-2-4 .next-no-fade').click(function () {
    let current_fs, next_fs, next_sub_step; //fieldsets
    current_fs = $(this).parent().parent().parent().parent();
    next_fs = $(this).parent().parent().parent().parent().next();
    next_sub_step = $(this).parent().parent().parent().parent().next().find('.right-col-form');
    current_fs.hide();
    next_fs.fadeIn(0);
    next_sub_step.hide();
    next_sub_step.fadeIn(500);
    $('html, body').animate({scrollTop: 0}, 500);
    $('#progress-home-step-2 .progress-bar').css('width', '70%');
    return false;
});

$('#submit-home-2-5 .next-no-fade').click(function () {
    let current_fs, next_fs, next_sub_step; //fieldsets
    current_fs = $(this).parent().parent().parent().parent();
    next_fs = $(this).parent().parent().parent().parent().next();
    next_sub_step = $(this).parent().parent().parent().parent().next().find('.right-col-form');
    current_fs.hide();
    next_fs.fadeIn(0);
    next_sub_step.hide();
    next_sub_step.fadeIn(500);
    $('html, body').animate({scrollTop: 0}, 500);
    $('#progress-home-step-2 .progress-bar').css('width', '85%');
    return false;
});

$('#submit-home-2-6 .next').click(function () {
    $('#progress-home-step-2 .progress-bar').css('width', '100%');
    $('#progress-home-step-3 .progress-bar').css('width', '10%');
    $('#progress-home-step-3 button').removeAttr('disabled');
});

$('#submit-home-3 .next').click(function () {
    $('#progress-home-step-3 .progress-bar').css('width', '100%');
    $('#progress-home-step-4 .progress-bar').css('width', '10%');
    $('#progress-home-step-4 button').removeAttr('disabled');
});

$('#progress-home-step-1 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-1').fadeIn(300);
});

$('#progress-home-step-2 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-2-1').fadeIn(300);
});

$('#progress-home-step-3 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-3').fadeIn(300);
});

$('#progress-home-step-4 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-4').fadeIn(300);
});

//inside form js
$('#home-form-2 input[type=radio][name=home-closing-date]').change(function () {
    $('#home-form-2 .nice-select-custom').addClass('text-muted').removeClass('select-active');
    $('#home-form-2 .nice-select-custom option').prop('selected', function () {
        return this.defaultSelected;
    });
    $('#home-form-2 .nice-select-custom').niceSelect('update');
});

$('#home-form-2').on('change', 'select', function () {
    $('#home-form-2 .check-icons .active').removeClass('d-none');
    $('#home-form-2 .check-icons .inactive').addClass('d-none');
    $('#home-form-3').find('.form-disabled').css('height', '0');
    $('input[type=radio][name=home-closing-date]').prop('checked', false);
    $('input[type=radio][name=home-closing-date]').parent().removeClass('focus active');
});

$('input[type=radio][name=another-mortgage-information]').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextFieldHide = $(this).parent().parent().parent().parent().parent().next('.js-form-field-hide');
    $('#submit-home-1').find('.form-disabled').css('height', '0');
    if (this.value === 'Yes') {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).removeClass('d-none');
        $(nextFieldHide).find('.form-disabled').css('height', '0');
        $('html, body').animate({
            scrollTop: nextFieldHide.offset().top - 200
        }, 500);
    }
    else if (this.value === 'No') {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).addClass('d-none');
    }
});

$('#home-form-40').on('change', 'select', function () {
    $('#home-form-40 .check-icons .active').removeClass('d-none');
    $('#home-form-40 .check-icons .inactive').addClass('d-none');
    $('#home-form-41').find('.form-disabled').css('height', '0');
});

$('#home-form-49').on('change', 'select', function () {
    $('#home-form-49 .check-icons .active').removeClass('d-none');
    $('#home-form-49 .check-icons .inactive').addClass('d-none');
    $('#home-form-50').find('.form-disabled').css('height', '0');
});

$('input[type=radio][name=select-mailing-address]').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextFieldHide = $(this).parent().parent().parent().parent().parent().next('.js-form-field-hide'),
        nextField = $(this).parent().parent().parent().parent().parent().next().next('.js-form-field-after-hide');
    if (this.value === 'add') {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).removeClass('d-none');
        $(nextField).find('.form-disabled').css('height', '0');
        $('html, body').animate({
            scrollTop: nextFieldHide.offset().top - 200
        }, 500);
    }
    else {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextFieldHide).addClass('d-none');
        $(nextField).find('.form-disabled').css('height', '0');
        $('html, body').animate({
            scrollTop: nextField.offset().top - 200
        }, 500);
    }
});

$('.js-next-field-input-in-hide').bind('keypress', function(e) {
    let icon = $(this).parent().parent().parent().parent().find('.check-icons'),
        nextField = $('#submit-home-3');
    if (e.keyCode == 13 && $(this).val()) {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextField).find('.form-disabled').css('height', '0');
        $('html, body').animate({
            scrollTop: nextField.offset().top - 200
        }, 500);
    }
});

$('input[type=radio][name=have-home-insurance]').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextField = $('#home-form-48');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');

    if (this.value === 'not-require-insurance') {
        $('#home-insurance-expired').addClass('d-none');
        $('#home-insurance-yes').addClass('d-none');
        $('html, body').animate({
            scrollTop: nextField.offset().top - 200
        }, 500);
        $(nextField).find('.form-disabled').css('height', '0');
    }
    if (this.value === 'expired-insurance') {
        $('#home-insurance-expired').removeClass('d-none');
        $('#home-insurance-yes').addClass('d-none');
        $('html, body').animate({
            scrollTop: $('#home-insurance-expired').offset().top - 200
        }, 500);
        $(nextField).find('.form-disabled').css('height', '100%');
    }
    if (this.value === 'have-insurance') {
        $('#home-insurance-expired').addClass('d-none');
        $('#home-insurance-yes').removeClass('d-none');
        $('html, body').animate({
            scrollTop: $('#home-insurance-yes').offset().top - 200
        }, 500);
        $('#home-form-47-4').find('.form-disabled').css('height', '0');
        $(nextField).find('.form-disabled').css('height', '100%');
    }
});

$('#home-form-47-1').on('change', 'select', function () {
    $('#home-form-47-1 .check-icons .active').removeClass('d-none');
    $('#home-form-47-1 .check-icons .inactive').addClass('d-none');
    $('#home-form-47-2').find('.form-disabled').css('height', '0');
});

$('input[type=radio][name=homeowners-policies-canceled]').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextField = $('#home-form-48');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');
    $(nextField).find('.form-disabled').css('height', '0');
    $('html, body').animate({
        scrollTop: nextField.offset().top - 200
    }, 500);
});

$('#home-form-47-5').on('change', 'select', function () {
    $('#home-form-47-5 .check-icons .active').removeClass('d-none');
    $('#home-form-47-5 .check-icons .inactive').addClass('d-none');
    $('#home-form-47-6').find('.form-disabled').css('height', '0');
});

$('input[type=radio][name=have-auto-insurance-included]').change(function () {
    let icon = $(this).parent().parent().parent().parent().parent().find('.check-icons'),
        nextField = $('#home-form-48');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');
    $('html, body').animate({
        scrollTop: nextField.offset().top - 200
    }, 500);
    $(nextField).find('.form-disabled').css('height', '0');
});

$('#home-form-51').on('change', 'select', function () {
    $('#home-form-51 .check-icons .active').removeClass('d-none');
    $('#home-form-51 .check-icons .inactive').addClass('d-none');
    $('#home-form-52').find('.form-disabled').css('height', '0');
});

let totalClicksDog = 3;
$('#clone-dog').click(function () {
    if (totalClicksDog > 1) {
        $(this).prev().clone(true).insertBefore(this);
        totalClicksDog = totalClicksDog-1;
    }
    if (totalClicksDog <= 1) {
        $('#clone-dog').addClass('d-none');
    }
    else {
        return false;
    }
    $('#home-form-37 .nice-select-update').niceSelect('update');
    return false;
});

let totalClicksLivestock = 3;
$('#clone-livestock').click(function () {
    if (totalClicksLivestock > 1) {
        $(this).prev().clone(true).insertBefore(this);
        totalClicksLivestock = totalClicksLivestock-1;
    }
    if (totalClicksLivestock <= 1) {
        $('#clone-livestock').addClass('d-none');
    }
    else {
        return false;
    }
    $('#home-form-37 .nice-select-update').niceSelect('update');
    return false;
});