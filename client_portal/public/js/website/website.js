/*
    Work on toolbar
*/

frappe.provide("frappe.ui.toolbar");
frappe.ui.toolbar.clear_cache = frappe.utils.throttle(function() {
        //frappe.assets.clear_local_storage();
        frappe.xcall('frappe.sessions.clear').then(message => {
                frappe.show_alert({
                        message: message,
                        indicator: 'green'
                });
        location.reload(true);
        }).then((message)=>{
            /*
            var links = document.getElementsByTagName('link');

            for (var i=0; i<links.length; i++) {
              let temp = links[i];
              if (temp.rel.toLowerCase().match(/stylesheet/) && temp.href) {
                let g = temp.href.replace(/(&|\?)rnd=\d+/, '');
                f.href = g + (g.match(/\?/) ? '&' : '?');
                f.href += 'rnd=' + (new Date().valueOf());
              }
            }
            */
        });
}, 10000);
