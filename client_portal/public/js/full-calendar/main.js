import {Calendar, formatDate} from 'node_modules/@fullcalendar/core';
import interactionPlugin from 'node_modules/@fullcalendar/interaction';
import bootstrapPlugin from 'node_modules/@fullcalendar/bootstrap';
import dayGridPlugin from 'node_modules/@fullcalendar/daygrid';
import timeGridPlugin from 'node_modules/@fullcalendar/timegrid';


frappe.provide("BondCalendar");

BondCalendar = class BondCalendar{
    constructor(ele, options){
        $.extend(options, {
            "plugins": this.get_plugins()
        })
        return new Calendar(ele, options);
    }

    get_plugins(){
        return [dayGridPlugin, timeGridPlugin, interactionPlugin, bootstrapPlugin];
    }

    formatDate(date, format){
        return formatDate(date, format);
    }
};
