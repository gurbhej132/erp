frappe.ui.form.ControlInput = frappe.ui.form.Control.extend({
	horizontal: true,
	make: function() {
		// parent element
		this._super();
		this.set_input_areas();

		// set description
		this.set_max_width();
	},
	make_wrapper: function() {
		this.only_input = true;

		this.$wrapper = $(`<div class="form-padding px-0 mx-0 row justify-content-center">
							<div class="js-form-field col-12 col-lg-11 col-xl-9">
								<div class="d-flex mb-4">
									<div class="check-icons"></div>
								<div>
								<div class="field-label"></div>
								<div class="field-description"></div>
							</div>
							<div class="d-flex align-items-center mb-4 input-main-wrapper">
								<div class="control-tooltip"></div>
								<div class="control-input-field input-group input-group-lg input-group-lg-control border border-dark rounded-pill control-input-field-wrapper">
									<div class="control-input-value"></div>
									<div class="prepend-icon"></div>
								</div>
							</div>
						</div>`).appendTo(this.parent);


	},
	toggle_label: function(show) {
		this.$wrapper.find(".field-label").toggleClass("hide", !show);
	},
	toggle_description: function(show) {
		this.$wrapper.find(".field-description").toggleClass("hide", !show);
	},
	set_input_areas: function() {
		this.label_area = this.$label;
		this.label_span = this.$label;
		this.input_area = this.$wrapper.find(".control-input-field").get(0);
		this.$input_wrapper = this.$wrapper.find(".control-input-field-wrapper");
		// keep a separate display area to rendered formatted values
		// like links, currencies, HTMLs etc.
		this.disp_area = this.$wrapper.find(".control-input-value").get(0);
	},
	set_max_width: function() {
		if(this.horizontal) {
			this.$wrapper.addClass("input-max-width");
		}
	},

	// update input value, label, description
	// display (show/hide/read-only),
	// mandatory style on refresh
	refresh_input: function() {
		var me = this;
		var make_input = function() {
			if (!me.has_input) {
				me.make_input();
				if (me.df.on_make) {
					me.df.on_make(me);
				}
			}
		};

		var update_input = function() {
			if (me.doctype && me.docname) {
				me.set_input(me.value);
			} else {
				me.set_input(me.value || null);
			}
		};

		if (me.disp_status != "None") {
			// refresh value
			if (me.frm) {
				me.value = frappe.model.get_value(me.doctype, me.docname, me.df.fieldname);
			} else if (me.doc) {
				me.value = me.doc[me.df.fieldname];
			}

			if (me.can_write()) {
				me.disp_area && $(me.disp_area).toggle(false);
				$(me.input_area).toggle(true);
				me.$input && me.$input.prop("disabled", false);
				make_input();
				update_input();
			} else {
				if (me.only_input) {
					make_input();
					update_input();
				} else {
					$(me.input_area).toggle(false);
					if (me.disp_area) {
						me.set_disp_area(me.value);
						$(me.disp_area).toggle(true);
					}
				}
				me.$input && me.$input.prop("disabled", true);
			}

			me.set_description();
			me.set_label();
			me.set_description();
			me.set_icons()
			me.set_tooltip();
			me.set_mandatory(me.value);
			me.set_bold();
		}
	},

	can_write() {
		return this.disp_status == "Write";
	},

	set_disp_area: function(value) {
		if(in_list(["Currency", "Int", "Float"], this.df.fieldtype)
			&& (this.value === 0 || value === 0)) {
			// to set the 0 value in readonly for currency, int, float field
			value = 0;
		} else {
			value = this.value || value;
		}
		if (this.df.fieldtype === 'Data') {
			value = frappe.utils.escape_html(value);
		}
		let doc = this.doc || (this.frm && this.frm.doc);
		let display_value = frappe.format(value, this.df, { no_icon: true, inline: true }, doc);
		this.disp_area && $(this.disp_area).html(display_value);
	},

	bind_change_event: function() {
		var me = this;
		this.$input && this.$input.on("change", this.change || function(e) {
			me.parse_validate_and_set_in_model(me.get_input_value(), e);
		});
	},
	bind_focusout: function() {
		// on touchscreen devices, scroll to top
		// so that static navbar and page head don't overlap the input
		if (frappe.dom.is_touchscreen()) {
			var me = this;
			this.$input && this.$input.on("focusout", function() {
				if (frappe.dom.is_touchscreen()) {
					frappe.utils.scroll_to(me.$wrapper);
				}
			});
		}
	},
	set_label: function(label) {

		this.$label = this.$wrapper.find(".field-label")
		if(this.df.label){
			$(`<p class="form-font">${this.df.label}</p>`).appendTo(this.$label);
		}
	},
	set_description: function(description) {

		this.$description = this.$wrapper.find(".field-description");

		if (description !== undefined) {
			this.df.description = description;
		}
		if (this.only_input || this.df.description===this._description) {
			return;
		}
		if (this.df.description) {
			this.$description.html(`<p class="font-description mt-2 mb-0 text-left text-muted">${this.df.description}</p>`);
		} else {
			this.set_empty_description();
		}
		this._description = this.df.description;
	},
	set_new_description: function(description) {
		this.$description.html(`<p class="font-description mt-2 mb-0 text-left text-muted">${this.df.description}</p>`);
	},
	set_empty_description: function() {
		this.$description.html("");
	},
	set_mandatory: function(value) {
		this.$wrapper.toggleClass("has-error", (this.df.reqd && is_null(value)) ? true : false);
	},
	set_bold: function() {
		if(this.$input) {
			this.$input.toggleClass("bold", !!(this.df.bold || this.df.reqd));
		}
		if(this.disp_area) {
			$(this.disp_area).toggleClass("bold", !!(this.df.bold || this.df.reqd));
		}
	},

	set_icons: function(){
		if(this.df.icons){
			this.$icons = this.$wrapper.find(".check-icons");
			this.$icons.addClass("mr-3")
			$.each(this.df.icons, (idx, icon)=>{
				$(icon).appendTo(this.$icons);
			});
		}

		if(this.df.editable_icon){
			this.$wrapper.find(".prepend-icon").html(`
					<span class="input-group-text">
						<span class="circle-icon">
							<i class="fas fa-pen"></i>
						</span>
					</span>`).addClass("input-group-prepend");
		}
	},

	set_tooltip: function(){
		this.$tooltip = this.$wrapper.find(".control-tooltip");
		if(this.df.tooltip){
			this.$tooltip.html($(`<div id="js-vin-tooltip" class="mr-1" data-toggle="tooltip" data-html="true"  data-original-title="<p>${this.df.tooltip}</p>">
									<img src="/assets/client_portal/images/icons/vin-icon.svg" alt="" class="img-fluid">
								</div>`));
			this.$tooltip.tooltip('enable');
		}
	}
});
