import 'frappe/public/js/frappe/form/controls/check';

frappe.ui.form.ControlCheck = frappe.ui.form.ControlCheck.extend({
	input_type: "checkbox",
	make_wrapper: function() {
		this.only_input=true;
		if(this.df.is_radio){
			this.$wrapper = $(`<div class="js-form-field col-12 col-lg-11 col-xl-9 mb-5 pb-4">
								<div class="d-flex mb-4">
									<div class="check-icons mr-3">
										<img class="active " src="/images/active-check.svg" alt="">
										<img class="inactive" src="/images/inactive-check.svg" alt="">
									</div>
									<p class="form-font">${this.df.label}?</p>
								</div>
								<div class="btn-group-toggle btn-group-lg d-flex justify-content-between w-100" data-toggle="buttons">
									<label class="w-48 d-flex justify-content-between align-items-center btn btn-outline-warning rounded-pill">
										<input type="radio" class="input-area" name="${this.df.fieldname}" value="1">Yes
										<span class="circle-icon"><i class="fas fa-star"></i></span>
									</label>
									<label class="w-48 d-flex justify-content-between align-items-center btn btn-outline-warning rounded-pill">
										<input type="radio" class="input-area" name="${this.df.fieldname}" value="0">No
										<span class="circle-icon"><i class="fas fa-star"></i></span>
									</label>
								</div>
							</div>`).appendTo(this.parent);
			}else{
				this.$wrapper = $(`<div class="js-form-field col-12 col-lg-11 col-xl-9 mb-5 pb-4">
										<div class="btn-group-toggle btn-group-lg mb-4" data-toggle="buttons">
											<label class="d-flex justify-content-between align-items-center btn btn-outline-warning rounded-pill">
												<input type="checkbox" class="input-area js-next-field-checkbox">
												${this.df.label}
												<span class="circle-icon"><i class="fas fa-star"></i></span>
											</label>
										</div>
									</div>`).appendTo(this.parent);

			}
			this.$input = this.$wrapper.find("input");
	},
	set_input_areas: function() {
		this.label_area = this.label_span = this.$wrapper.find("label").get(0);
		this.input_area = this.$wrapper.find("input");
		this.disp_area = this.$wrapper.find(".js-form-field").get(0);
	},
	make_input: function() {
		//this._super();
		if(!this.$input){

			this.set_input_attributes();
			this.input = this.$input;
			this.has_input = true;
			this.bind_change_event();
			this.bind_focusout();
			this.setup_autoname_check();
		}
		this.$input.removeClass("form-control");
	},
	get_input_value: function() {
		return this.input && this.input.checked ? 1 : 0;
	},
	validate: function(value) {
		return cint(value);
	},
	set_input: function(value) {
		value = cint(value);
		if(this.input) {
			this.input.checked = (value ? 1 : 0);
		}
		this.last_value = value;
		this.set_mandatory(value);
		this.set_disp_area(value);
	},
	bind_change_event: function(){
		this.$wrapper.find('input').change(function(){
			console.log(this.value);
		});
	}
});
