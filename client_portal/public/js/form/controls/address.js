frappe.ui.form.ControlAddress = frappe.ui.form.ControlInput.extend({
	html_element: "input",
	input_type: "text",
	only_input: true,
	make_input: function() {
		if(this.field_group){
			return
		}

		this.$input = $(`<div></div>`);
		this.field_group = new frappe.ui.FieldGroup({
			"parent": this.$wrapper,
			"fields": this.get_fields()
		});
		this.has_input = true;
		this.input = this.$input;
		this.field_group.make();
		this.make_section_text_center();
		if(this.df.value){
			frappe.call({
				"method": "bondportal.utils.form.get_address",
				"args":{
					"address": this.df.value,
				},
				"callback": (res)=>{
					if(res && res.message){
						this.set_default_values(res.message);
					}
				}
			});
		}
	},
	make_section_text_center: function(){
		$.each(this.field_group.fields_dict, (key, field)=>{
			if(!in_list(["Section Break", "Column Break"], field.df.fieldtype)){
				field.$input.attr("placeholder", field.df.label);
			}
		});
	},

	get_fields: function(){
		return [{
			"fieldtype": "Section Break", "label": this.df.label, "cssClass": "text-center"
		},{
			"fieldtype": "Data", "fieldname": "street_name", "label": __("Street Name"), "reqd": 1,
			"palceholder": __("Street Name"),
		},{
			"fieldtype": "Section Break",
		},{
			"fieldtype": "Data", "fieldname": "apartment_no", "label": __("Apt, Suite, etc"), "reqd": 1,
		},{
			"fieldtype": "Column Break"
		},{
			"fieldtype": "Link", "fieldname": "city", "label": __("City"), "reqd": 1, "options": "City",
		},{
			"fieldtype": "Section Break"
		},{
			"fieldtype": "Link", "fieldname": "state", "label": __("State"), "reqd": 1, "options": "State",
		},{
			"fieldtype": "Column Break"
		},{
			"fieldtype": "Data", "fieldname": "zip_code", "label": __("Zip Code"), "reqd": 1,
		}];
	},

	setup_autoname_check: function() {
		if (!this.df.parent) return;
		this.meta = frappe.get_meta(this.df.parent);
		if (this.meta && ((this.meta.autoname
			&& this.meta.autoname.substr(0, 6)==='field:'
			&& this.meta.autoname.substr(6) === this.df.fieldname) || this.df.fieldname==='__newname') ) {
			this.$input.on('keyup', () => {
				this.set_description('');
				if (this.doc && this.doc.__islocal) {
					// check after 1 sec
					let timeout = setTimeout(() => {
						// clear any pending calls
						if (this.last_check) clearTimeout(this.last_check);

						// check if name exists
						frappe.db.get_value(this.doctype, this.$input.val(),
							'name', (val) => {
								if (val) {
									this.set_description(__('{0} already exists. Select another name', [val.name]));
								}
							},
							this.doc.parenttype
						);
						this.last_check = null;
					}, 1000);
					this.last_check = timeout;
				}
			});
		}
	},
	set_input_attributes: function() {
		this.$input
			.attr("data-fieldtype", this.df.fieldtype)
			.attr("data-fieldname", this.df.fieldname)
			.attr("placeholder", this.df.placeholder || "");
		if(this.doctype) {
			this.$input.attr("data-doctype", this.doctype);
		}
		if(this.df.input_css) {
			this.$input.css(this.df.input_css);
		}
		if(this.df.input_class) {
			this.$input.addClass(this.df.input_class);
		}
	},
	set_input: function(value) {
		this.last_value = this.value;
		this.value = value;
		this.set_formatted_input(value);
		this.set_disp_area(value);
		this.set_mandatory && this.set_mandatory(value);
	},
	set_formatted_input: function(value) {
		this.$input && this.$input.val(this.format_for_input(value));
	},
	get_input_value: function(ignore_errors=false) {
		var ret = {};
		var errors = [];
		for(var key in this.field_group.fields_dict) {
			var f = this.field_group.fields_dict[key];
			if(f.get_value) {
				var v = f.get_value();
				if(f.df.reqd && is_null(v))
					errors.push(__(f.df.label));

				if(!is_null(v)) ret[f.df.fieldname] = v;
			}
		}
		return ret;
	},
	format_for_input: function(val) {
		return val==null ? "" : val;
	},
	validate: function(v) {
		if(this.df.is_filter) {
			return v;
		}
		if(this.df.options == 'Phone') {
			if(v+''=='') {
				return '';
			}
			var v1 = '';
			// phone may start with + and must only have numbers later, '-' and ' ' are stripped
			v = v.replace(/ /g, '').replace(/-/g, '').replace(/\(/g, '').replace(/\)/g, '');

			// allow initial +,0,00
			if(v && v.substr(0,1)=='+') {
				v1 = '+'; v = v.substr(1);
			}
			if(v && v.substr(0,2)=='00') {
				v1 += '00'; v = v.substr(2);
			}
			if(v && v.substr(0,1)=='0') {
				v1 += '0'; v = v.substr(1);
			}
			v1 += cint(v) + '';
			return v1;
		} else if(this.df.options == 'Email') {
			if(v+''=='') {
				return '';
			}

			var email_list = frappe.utils.split_emails(v);
			if (!email_list) {
				// invalid email
				return '';
			} else {
				var invalid_email = false;
				email_list.forEach(function(email) {
					if (!validate_email(email)) {
						frappe.msgprint(__("Invalid Email: {0}", [email]));
						invalid_email = true;
					}
				});

				if (invalid_email) {
					// at least 1 invalid email
					return '';
				} else {
					// all good
					return v;
				}
			}

		} else {
			return v;
		}
	},
	set_default_values: function(address){
		if(!address)
			return;

		// Set the default values for the form
		this.field_group.set_value("street_name", address.address_line1);
		this.field_group.set_value("apartment_no", address.address_line2);
		this.field_group.set_value("city", address.city);
		this.field_group.set_value("state", address.state);
		this.field_group.set_value("zip_code", address.pincode);
	}
});
