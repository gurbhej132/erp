/*
    ContactUs Container
*/
frappe.provide("contactus");

frappe.ready(()=>{

    class ContactUs {
        constructor(args){
            $.extend(this, args);
            this.make()
        }

        make(){
            this.init_handlers();
        }

        init_handlers(){
            $(".form-contactus").on("submit", (e)=>{

                $(".error-message").empty();

                let args = {
                    "message": $("#message").val(),
                    "first_name": $("#first_name").val(),
                    "last_name": $("#last_name").val(),
                    "email": $("#email").val()
                };

                if(!args.message){
                    $(`<span>${__("Message is required")}</span>`).appendTo($(".error-message"));
                    return false;
                }

                if(!args.first_name || !args.last_name){
                    $(`<span>${__("First/Last name is required")}</span>`).appendTo($(".error-message"));
                    return false;
                }

                if(!args.email){
                    $(`<span>${__("Email is required")}</span>`).appendTo($(".error-message"))
                    return false;
                }

                frappe.call({
                    "method": "bondportal.www.contactus.index.contactus",
                    "args":{
                        args: args,
                    },
                    "btn": $("submit"),
                    "freeze": true,
                    "quite": true,
                    "statusCode":{
                        200: (res)=>{
                            $(".contactus-container").html(
                                $(`<h4>Thank you for contacting us</h4>
                                    <a class='btn btn-success' href='/dashboard'>${__("Go Back")}</a>`));

                        },
                        417:(res)=>{
                            res = JSON.parse(res.responseJSON._server_messages);
                            if(res){
                                let errors = "";
                                for(var i=0;i<res.length;i++){
                                    let temp = JSON.parse(res[i]);
                                    $(`<span>${temp.message}</span>`).appendTo($(".error-message"));
                                }
                            }
                        }
                    }
                });


                return false;
            });
        }
    };

    new ContactUs({})
});
