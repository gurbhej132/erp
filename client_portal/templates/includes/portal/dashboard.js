/*
    Base Dasboard functionality for all the dashbaord on Portal
*/

/*
    Standard Sidebar functionality for the dashboard
*/
class Sidebar{

    constructor(args){
        this.args = args;
        this.make();
    }

    make(){
        // Sidebar container
        this.$sidebar = $(".sidebar");
        // User information container
        this.$user_information_wrapper = this.$sidebar.find(".sidebar-user-information");
        // Sidebar options container
        this.$options_wrapper = this.$sidebar.find(".sidebar-options");
        // Sidebar carousel container
        this.$sidebar_carousel_wrapper = this.$sidebar.find(".sidebar-carousel");

        // Init and make sidebar container
        this.make_user_profile();
        this.make_sidebar_options();
        this.make_carousel();
    }

    // Make user profile here
    make_user_profile(){
        let user_image = frappe.boot.bondportal.user_image?frappe.boot.bondportal.user_image:"/assets/bondportal/images/icons/welcome-icon.svg";
        this.$user_image_wrapper = $(`<div class="col-12 col-lg-5 text-center">
            <img class="img-fluid" src="${user_image}" alt="{{title}}">
        </div>`).appendTo(this.$user_information_wrapper);

        this.$user_wrapper = $(`<div class="col-12 col-lg-10 text-center"><h3 class="mt-4 mb-4">${frappe.boot.bondportal.full_name}</h3>
        </div>`).appendTo(this.$user_information_wrapper);

        // Click to call wrapper
        this.$click_to_call_wrapper = $(`<div class="col-12 col-lg-10 text-center pl-4 pr-4">
                <a href="tel:1234567890" class="click-to-call d-block ml-xl-5 mr-xl-5  text-dark">
                    ${__("Click to Call")}
                </a>
                <a href="#" class="click-to-chat d-block ml-xl-5 mr-xl-5 mb-xl-5 text-dark">
                    ${__("Chat")}
                    <span class='text-warning'>
                        <svg width=0.5em height=0.5em viewBox="0 0 16 22" class="bi bi-circle-fill" fill=currentColor xmlns=http://www.w3.org/2000/svg><circle cx=8 cy=8 r=8/>
                        </svg>
                    </span>
                </a>
        </div>`).appendTo(this.$user_information_wrapper);

        // Handler for click to call wrapper
        this.$click_to_call_wrapper.find(".click-to-call").on("click", (e)=>{
            return true;
        });

        // Handle click to chat button
        this.$click_to_call_wrapper.find(".click-to-chat").on("click", (e)=>{
            frappe.msgprint(__("Feature is coming soon"));
            return false;
        });


        // User account wrapper
        this.$my_account_wrapper = $(`<div class="col-12 col-lg-10 text-center">
            <button type=button class="btn btn-outline-dark mt-4 mb-5 btn-my-account">${__("My Account")}</button>
        </div>`).appendTo(this.$user_information_wrapper);

        // Handler for user account button
        this.$my_account_wrapper.find(".btn-my-account").on("click", (e)=>{
            window.location.href = "/settings";
        });
    }

    make_sidebar_options(){
        this.make_apply_for_options();
        this.make_your_services_options();
    }

    make_sidebar_options(){


        this.options = {};
        frappe.call({
            "method": "bondportal.www.dashboard.index.get_dashboard_options",
            "args":{
            },
            "callback": (res)=>{
                if(res && res.message){
                    var me = this;
                    $.each(res.message, (idx, option)=>{
                        let $options_wrapper = $(`<div class=" sidebar-options col-12 col-lg-10 mt-2 mb-2">
                        </div>`).appendTo(this.$options_wrapper);
                        this.make_options(option, $options_wrapper)
                    });
                }
            }
        });

    }

    make_options(option, $options_wrapper){

        let label = frappe.scrub(option.label);
        let $option_wrapper = $(`<div class="">
            <a class="form-control d-flex justify-content-between pl-0 pr-0"
                data-toggle=collapse href="#${label}" aria-expanded=false aria-controls='${label}'>
                <div class=font-weight-bold>${option.label}</div>
                <div class=text-right>
                    <svg width=0.7em height=0.9em viewBox="0 0 16 16" class="bi bi-caret-down-fill" fill=currentColor xmlns=http://www.w3.org/2000/svg>
                        <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                    </svg>
                </div></a>
                <div class="collapse ml-2 mt-2 options" id="${label}"></div>
        </div>`).appendTo($options_wrapper);


        $.each(option.items, (idx, val)=>{

            let sub_label = frappe.scrub(val.label);
            if(!val.has_sub_options){
                $(`<a href="${val.link}" class="d-block">${val.label}</a>
                `).appendTo($option_wrapper.find(".options"));
            }else{
                let $sub_option_wrapper = $(`<div>
                    <a class="d-flex justify-content-between" data-toggle=collapse href=#${sub_label} aria-expanded=false aria-controls=${sub_label}>
                    <div class="">${val.label}</div>
                    <div class=text-right>
                        <svg width=0.7em height=0.9em viewBox="0 0 16 16" class="bi bi-caret-down-fill" fill=currentColor xmlns=http://www.w3.org/2000/svg>
                            <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                        </svg>
                    </div>
                </a>
                <div class="sub-options collapse ml-2" id=${sub_label}></div>
                </div>`).appendTo($option_wrapper.find(".options"));
                $.each(val.sub_options[0].items, (sub_idx, sub_option)=>{
                    $(`<a href='${sub_option.link}' class='d-block'>${sub_option.label}</a>`).appendTo($sub_option_wrapper.find(".sub-options"));
                });
            }
        });
    }

    make_carousel(){
        $(`<div id=carouselAsideControls class="carousel slide col-12 col-lg-10" data-ride=carousel>
    			<div class="carousel-inner">
    				<div class="carousel-item active text-center">
    					<button type=button class="feedback btn btn-outline-dark">${__("Feedback")}</button>
    				</div>
    				<div class="carousel-item text-center">
    					<button type="button" class="contact-us btn btn-outline-dark">${__("Contact Bond")}</button>
    				</div>
    			</div>
    			<a class="carousel-control-prev" href="#carouselAsideControls" role=button data-slide=prev>
    				<span class="text-dark font-weight-bold large">
    					<svg width=0.8em height=0.8em viewBox="0 0 16 18" class="bi bi-caret-left-fill" fill=currentColor xmlns=http://www.w3.org/2000/svg>
    						<path d="M3.86 8.753l5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z"/>
    					</svg>
    				</span>
    			</a>
    			<a class=carousel-control-next href=#carouselAsideControls role=button data-slide=next>
    				<span class="text-dark font-weight-bold">
    					<svg width=0.8em height=0.8em viewBox="0 0 16 18" class="bi bi-caret-right-fill" fill=currentColor xmlns=http://www.w3.org/2000/svg>
    						<path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
    					</svg>
    				</span>
    			</a>
    	</div>`).appendTo(this.$sidebar_carousel_wrapper);

        //  Feedback handler
        this.$sidebar_carousel_wrapper.find(".feedback").on("click", (e)=>{
            window.location.href="/feedback";
            return false;
        });

        // Contact Us handler
        this.$sidebar_carousel_wrapper.find(".contact-us").on("click", (e)=>{
            window.location.href="/contactus";
            return false;
        });
    }
};
/*
    End of sidebar functionality
*/

/*
    Standard Dashboard functionality for the dashbaord
*/
class Dashboard{

    constructor(args){
        this.args = args;
        this.make();
    }

    make(){
        this.make_sidebar();
        this.init_handler();
    }

    make_sidebar(){
        this.sidebar = new Sidebar({
            "dashboard": this,
        });

    }
    init_handler(){
        this.handle_google_search();
        this.handle_platform_search();
        this.make_default_homepage();
    }

    handle_google_search(){

        // Disable the search for submission
        $(".dashboard-search-form").on("submit", ()=>{
            return false;
        });

        // If user press enter in search box
        $("#dashboard_search_input").on("keypress", (e)=>{
            $("#platform_search").click();
            return false;
        });

        // If user press the voice button
        $("#dashboard_search_btn").on("click", (e)=>{
            frappe.msgprint(__("Please speak"));
            return false;
        });

        // Click to call handler
        $("#click_to_call").on("click", (e)=>{

            return false;
        });

        // Click to chat
        $("#click_to_chat").on("click", (e)=>{
            return false;
        });
    }

    handle_platform_search(){
        $("#platform_search").on("click", ()=>{
            frappe.msgprint(__("Platform search btn clicked"));
            return false;
        });
    }

    make_default_homepage(){
        $("#make_default_homepage").on("click", (e)=>{
            let val  = $("#make_default_homepage:checked").val();
            frappe.msgprint(__("Make Default homepage button clicked"));
            // frappe.call to change the values in database here
        });
    }
};
/*
    End of the dashbaord functionality
*/
