/*
    Basic form related functionality
*/


frappe.provide("bondportal.settings.account");

Form  = class Form extends frappe.ui.FieldGroup{
    constructor(args){
        super();
        this.args = args;
        this.parent = args.parent;
		$.extend(this, { animate: true, size: null });
        $.extend(this, args);
        this.get_form_fields();
    }

    get_form_fields(){
        this.route = this.get_route();
        this.get_form_method = this.get_form_meta_method();
        this.save_form_method = this.get_form_save_method();

        frappe.call({
            "method": "bondportal.utils.form.get_form_meta",
            "args":{
                "route": this.route,
                "method": this.get_form_method,
            },
            "callback": (res)=>{
                if(res && res.message){
                    this.fields=res.message.fields;
                    this.make_form(res.message);
                }
            }
        });
    }

    // Make grid for multiple user information
    make_grid(form){
        this.grid = new Grid({
            form:form, _frm: this,
            parent: this.parent
        });
    }

    // Make the form and fields
    make_form(form){
        this.form = form;
        this.$main_parent = this.parent;
        this.parent = $(`<div class='form-wrapper'></div>`).appendTo(this.$main_parent);

        this.$form_title = $(`<div class='form-title'>
            <h3 class='text-center'>${form.form_title}</h3>
        </div>`).appendTo(this.parent);

        this.$validation_wrapper = $(`<div class=" text-warning validation-wrapper"></div>`).appendTo(this.$main_parent);
        this.$controls = $(`<div class='text-center mt-4 mb-4'></div>`).appendTo(this.$main_parent);

        this.init_control(form);

        //this.make_form();
        super.make();
        this.setup_handlers();
        this.sync_form();
        this.setup_placeholders();
    }

    // Setup handler in sub forms
    setup_handlers(){
        if(this.onload && $.isFunction(this.onload)){
            this.onload();
        }
    }

    // Init edit/save handler
    init_control(form){
        if(form.allow_update){
            this.$edit_control = $(`<button class='btn btn-primary edit-control mr-2'>
                ${__("Edit")}
            </button>`).appendTo(this.$controls);
            this.$save_control = $(`<button class='btn btn-warning save-button' disabled>
                ${__("Save")}
            </button>`).appendTo(this.$controls);

            // Init edit handler now
            this.$edit_control.on("click", (e)=>{
                this.edit_form();
                this.disable_edit_button();

                this.$save_control.prop("disabled", false);
                return false;
            });

            // init save handler now
            this.$save_control.on("click", (e)=>{
                this.save_form();
                return false;
            });

        }
    }

    // Setup palceholders as it doesn't support under frappe framwework
    setup_placeholders(){
        $.each(this.fields_dict, (fieldname, field)=>{

            if(!in_list(['Section Break', 'Column Break'], field.df.fieldtype) && field.$input){
                field.$input.attr("placeholder", field.df.label);
            }
            if(field.df.fieldtype == "Button"){
                field.$input.removeClass("btn-default").addClass("btn-warning");
            }
        });
    }
    // Get route
    get_route(){
        return window.location.pathname;
    }

    // Get method
    get_form_meta_method(){
        return this.args.get_form_method?this.args.get_form_method:"get_form";
    }

    get_form_save_method(){
        return this.args.save_form_method?this.args.save_form_method:"save_form";
    }

    edit_form(){
        $.each(this.fields_dict, (key, field)=>{
            this.set_df_property(key, "read_only", 0);
        });
    }

    sync_form(){
        if(this.fields_dict && this.form.values){
            $.each(this.fields_dict, (fieldname, field)=>{
                if(!in_list(["Section Break", "Column Break"], field.fieldtype)){
                    this.set_value(fieldname, this.form.values[fieldname]);
                }
            });
        }
    }

    save_form(){
        let values =  this.get_values();
        if(!values){
            return false;
        }

        // Update existing form
        if(this.form.values){
            $.extend({
                "name": this.form.values.name,
            });
        }
        let opts = {};
        frappe.call({
            "method": "bondportal.utils.form.save_form",
            "args":{
                "form": values,
                "route": this.route,
                "method": this.save_form_method || "save_form"
            },
            "freeze": true,
            "silent": true,
            "freeze_message": __("Saving Form"),
            "btn": $("btn"),
            "error_handlers":{
                "ValidationError": (res)=>{
                    this.handle_errors(res);
                }
            },
            "callback": (res)=>{
                // Update the updated values in form
                if(res && res.message && res.doc){
                    this.form.values = res.doc;
                }

                let title = __(this.form.form_title + " was updated successfully.");
                if(res && res.message && res.message.title){
                    title = res.message.title || title;
                }
                this.$validation_wrapper.empty();
                this.$validation_wrapper.html(`<h4 class='text-success'>
                    ${title}
                </h4>`);

                this.$validation_wrapper.find(".text-success").fadeOut(3000, (e)=>{
                    this.$validation_wrapper.empty();
                });


            }
        });
    }

    handle_errors(res){
        this.$validation_wrapper.empty();
        let _server_messages = JSON.parse(res._server_messages);
        let $error_wrapper = $(`<div class='text-warning'></div>`)
        if(_server_messages){
            $.each(_server_messages, (idx, err_msg)=>{
                err_msg = JSON.parse(err_msg);
                $(`<h4 class='text-warning'>${err_msg.message}</h4>`).appendTo($error_wrapper);
            });
            $error_wrapper.appendTo(this.$validation_wrapper);

            this.$validation_wrapper.find(".text-warning").fadeOut(5000, (e)=>{
                this.$validation_wrapper.empty();
            });
        }
    }

    disable_edit_button(){
        this.$edit_control.prop("disabled", true);
        this.$save_control.prop("disabled", false);
    }

    disable_save_button(){
        this.$save_control.prop("disabled", true);
        this.$edit_control.prop("disabled", false);
    }

}


class Grid{

    constructor(args){
        $.extend(this, args);

        // Boostrap datatable
        this.operateEvents = {
            'click .like': (e, value, row, index)=> {
                alert('You click like action, row: ' + JSON.stringify(row))
            },
            'click .remove': (e, value, row, index)=> {
                this.$table.bootstrapTable('remove', {
                    field: 'id',
                    values: [row.id]
                });
             }
         }

        this.make();
    }


    make(){
        this.$wrapper = this.parent;
        // Make toolbar wrapper

        this.$toolbar = $(`<div class='text-center mt-4 mb-4' id="toolbar">
                <button class='btn btn btn-danger mr-2' id="remove">
                    <i class="fa fa-trash"></i> ${__("Delete")}
                </button>
                <button class="btn btn-secondary mr-2" type="button" name="fullscreen" aria-label="Fullscreen" title="Fullscreen" id="fullscreen">
                    <i class="fa fa-arrows-alt"></i>
                </button>
                <button class="btn btn-secondary" type="button" name="fullscreen" aria-label="Fullscreen" title="Fullscreen" id="new">
                    <i class="fa fa-plus"></i> ${__("New")}
                </button>

        </div>`).appendTo(this.$wrapper);

        this.$search = $(`<div class='text-center mt-4 mb-4'>
            <input class="form-control search-input" type="text" placeholder="Search" autocomplete="off">
        </div>`).appendTo(this.$wrapper);



        // Make table wrapper
        this.table = $(`<table class="table"
                id="table" data-toolbar="#toolbar"
                data-minimum-count-columns="2"
                data-pagination="true" data-id-field="id"
                data-page-list="[10, 25, 50, 100, all]">
        </table>`).appendTo(this.$wrapper);


        this.make_handlers();
    }

    make_handlers(){
        this.$table = $("#table");
        this.$remove = this.$toolbar.find("#remove");
        this.$fullscreen =  this.$toolbar.find("#fullscreen");
        this.$new = this.$toolbar.find("#new");

        // Remove row from table
        this.$remove.on("click", ()=>{
            var ids = this.getIdSelections()
            this.$table.bootstrapTable('remove', {
                field: 'id',
                values: ids
            });
            this.$remove.prop('disabled', true)
        });

        // Full Screen
        this.$fullscreen.on("click", (e)=>{
            this.$table.bootstrapTable('toggleFullscreen');
        });

        // Add new business

        this.$new.on("click", (e)=>{
            console.log(e);
        });


        this.selections = [];
        this.initTable();

    }

    getIdSelections() {
        return $.map(this.$table.bootstrapTable('getSelections'), (row)=> {
            return row.id
        });
    }

    responseHandler(res) {
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.id, this.selections) !== -1
        });

        return res
    }

    detailFormatter(index, row) {
        var html = []
        $.each(row, (key, value)=> {
            html.push('<p><b>' + key + ':</b> ' + value + '</p>')
        });
        return html.join('')
    }

    operateFormatter(value, row, index) {
       return [
         '<a class="like" href="javascript:void(0)" title="Like">',
         '<i class="fa fa-heart"></i>',
         '</a>  ',
         '<a class="remove" href="javascript:void(0)" title="Remove">',
         '<i class="fa fa-trash"></i>',
         '</a>'
       ].join('')
   }

    totalTextFormatter(data) {
        return 'Total'
    }

    totalNameFormatter(data) {
        return data.length
    }

    totalPriceFormatter(data) {
        var field = this.field
        return '$' + data.map(function (row) {
            return +row[field].substring(1)
        }).reduce(function (sum, i) {
            return sum + i
        }, 0);
    }

    initTable() {
        var columns = this.get_columns();
        this.$table.bootstrapTable('destroy').bootstrapTable({
            height: 550,
            locale: "en-US",
            columns: [columns]
        });

        this.$table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table',
             ()=> {
                this.$remove.prop('disabled', !this.$table.bootstrapTable('getSelections').length)
                // save your data, here just save the current page
                this.selections = this.getIdSelections()
                // push or splice the selections if you want to save all data selections
        });

        this.$table.on('all.bs.table', (e, name, args)=> {
            console.log(name, args)
        });
        //$('#locale').change(initTable)
    }

    get_columns(){

        let columns = [];
        if(this.form.fields){
            $.each(this.form.fields, (idx, field)=>{

                if(!in_list(['Section Break', 'Column Break'], field.fieldtype)){
                    columns.push({
                        field: field.fieldname,
                        title: field.label,
                        sortable: true,
                        footerFormatter: this.totalNameFormatter,
                        align: 'center',
                        valign: 'middle',
                        clickToSelect: false,
                        events: this.operateEvents,
                        formatter: this.operateFormatter
                    });
                }
            });
        }
        return columns;
    }
};
