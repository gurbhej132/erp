/*
*/

let password_policy = new PasswordPolicy({
                        "$password_wrapper": $("#password"),
                        "$confirm_password_wrapper": $("#confirmpassword"),
                        "$error_message_wrapper": $(".error-message")
                    });


$(".form-signup").on("submit", (event)=>{
    event.preventDefault();
    let args = {};
    $.extend(args, {
            "first_name": $("#firstname").val(),
            "last_name": $("#lastname").val(),
            "email": $("#email").val(),
            "password": $("#password").val(),
            "confirmpassword": $("#confirmpassword").val(),
            "accept_terms": $("#acceptterms").is(":checked"),
    });

    if(!password_policy.is_password_policy_matched()){
        return false;
    }
    if(!password_policy.match_password(args.password, args.confirmpassword)){
        return false;
    }

    if(!password_policy.validate_mandatory()){
        return false;
    }

    frappe.call({
        "method": "bondportal.www.signup.index.signup",
        "args":{
            "args": args,
        },
        "freeze": true,
        "callback": (res)=>{
            $(".error-message").empty();
            let $msg =  $(`<label>${res.message.error_message}</label>`);
            if(res && res.message && res.message.error == true){
                $msg.appendTo($(".error-message"));
                return false;
            }
            frappe.call({
                "method": "login",
                "args":{
                    "usr": res.message.user.name,
                    "pwd": args.password
                },
                "callback": (res)=>{
                    if(res && res.redirect_to){
                        window.location.href= "/welcome"
                    }
                }
            })
        }
    })
    return false;
})
