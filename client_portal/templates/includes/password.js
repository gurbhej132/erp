/*
*/

PasswordPolicy = class PasswordPolicy{

    constructor(args){
        this.$password_wrapper = args.$password_wrapper;
        this.$confirm_password_wrapper = args.$confirm_password_wrapper;
        this.$newpassword_wrapper = args.$newpassword_wrapper;
        this.$error_message_wrapper = args.$error_message_wrapper;
        this.update_defaults();
    }

    update_defaults(){
        this.password_policy_matched = false;
        this.$error_message_wrapper.addClass("text-warning");
        this.init_handlers();
    }

    init_handlers(){
        // Sign up password wrapper
        if(this.$password_wrapper){
            this.$password_wrapper.on("keyup", (e)=>{
                let password = $(this.$password_wrapper).val();
                let confirm_password = $(this.$confirm_password_wrapper).val();
                if(confirm_password){
                    if(!this.match_password(password, confirm_password)){
                        return false;
                    };
                }
                this.test_password(password);
            });
        }
        if(this.$confirm_password_wrapper){
            this.$confirm_password_wrapper.on("keyup", (e)=>{
                let password = this.$password_wrapper.val();
                let confirm_password =  this.$confirm_password_wrapper.val();
                this.match_password(password, confirm_password);
            });
        }

        // Forgot password wrapper
        if(this.$newpassword_wrapper){
            this.$newpassword_wrapper.on("keyup", (e)=>{
                let new_password = $(this.$newpassword_wrapper).val();
                this.test_password(new_password);
            });
        }
    }

    test_password(password, confirmpassword){
        frappe.call({
            "method": "bondportal.www.update-password.index.test_password_strength",
            "args":{
                "password": password,
                "confirmpassword": confirmpassword
            },
            "callback": (res)=>{
                this.$error_message_wrapper.empty();
                this.password_policy_matched = true;
                if(res && res.message && !res.message.flag){
                    this.password_policy_matched = false;
                    $(res.message.error_message).appendTo(this.$error_message_wrapper);
                }else{
                    // Password matched
                    $(`<ul class='text-success'>
                            <b>The password must contain at least three character categories among the following:</b>
        					<li>Uppercase characters (A-Z)</li>
        					<li>Lowercase characters (a-z)</li>
        					<li>Digits (0-9)</li>
        					<li>Special characters (~!@#$%^&*_-+=\`|\(){}[]:;"'<>,.?/)</li><ul>`).appendTo(this.$error_message_wrapper)
                }
            }
        });
    }

    is_password_policy_matched(){
        return this.password_policy_matched;
    }

    match_password(password, confirmpassword){
        this.$error_message_wrapper.empty();
        if(password !== confirmpassword){
            $(`<span>
                    ${__('<b>The password and confirm password does not match</b>')}
            </span>`).appendTo(this.$error_message_wrapper);
            return false;
        }
        return true;
    }

    is_password_link_expired(){
        if(frappe.utils.get_url_arg("password_expired")) {
            this.$error_message_wrapper.empty();
    		$(`<span>${__('The password of your account has expired.')}</span>`).appendTo(this.$error_message_wrapper)
            return false;
    	}
        return true;
    }

    validate_old_password(args){
        if(!args.old_password && !args.key) {
            this.$error_message_wrapper.empty();
			$("<span>Old Password Required.</span>").appendTo(this.$error_message_wrapper);
            return false;
		}
        return true;
    }

    validate_new_password(args){
        if(!args.new_password) {
            this.$error_message_wrapper.empty();
            $(`<span>${__("New Password Required.")}</span>`).appendTo(this.$error_message_wrapper);
            return false;
        }
        return true;
    }

    // Validate mandatory password fields
    validate_mandatory(){
        if(this.$password_wrapper && !this.validate_form_mandatory_field(this.$password_wrapper)){
            return false;
        }
        if(this.$confirm_password_wrapper && !this.validate_form_mandatory_field(this.$confirm_password_wrapper)){
            return false;
        }
        if(this.$newpassword_wrapper && !this.validate_form_mandatory_field(this.$newpassword_wrapper)){
            return false;
        }
        return true;

    }

    // Validate form password field
    validate_form_mandatory_field($wrapper){
        let placeholder = $wrapper.attr("placeholder");
        let is_required = $wrapper.attr("required");
        let val = $wrapper.val();
        let flag = true;
        if(!val){
            this.$error_message_wrapper.empty();
            $(`<span>${__(placeholder)} is required<span>`).appendTo(this.$error_message_wrapper);
            flag =  false;
        }
        return flag;
    }
}
