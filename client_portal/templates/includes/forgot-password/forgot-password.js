// login.js
// don't remove this line (used in test)

window.disable_signup = {{ disable_signup and "true" or "false" }};

window.login = {};

window.verify = {};

$(".form-forgot-password").on("submit", function(event) {
    event.preventDefault();
    var args = {};
    args.cmd = "bondportal.www.forgot-password.index.reset_password";
    args.user = ($("#forgot_email").val() || "").trim();
    frappe.call({
        "method": args.cmd,
        "args":{
            "user": args.user
        },
        "callback": (res)=>{
            $(".error-message").empty();
            $(".password-changed").empty();
            if(res && res.message && res.message.error){
                $(`<span>${res.message.error_message}</span>`).appendTo($(".error-message"));
                return
            }
            $(".forgot-password-container").addClass("hidden");
            $(".password-changed").removeClass("hidden");
            $(`
                <h4>${__("Confirmation email sent to your email")}</h4>
                <a href='/'>${__("Go to home page")}</a>
            `).appendTo(".password-changed");
        }
    });

    return false;
});
