'''
    Overriding Standard Frappe Login Class
'''

import frappe
from frappe.auth import (HTTPRequest, LoginManager as FrappeLoginManager,
                CookieManager, check_session_stopped, validate_ip_address)
from frappe.translate import get_lang_code
from client_portal.www.login.index import get_login_redirect

class HTTPRequest(HTTPRequest):
    def __init__(self):
        # Get Environment variables
        self.domain = frappe.request.host
        if self.domain and self.domain.startswith('www.'):
            self.domain = self.domain[4:]

        if frappe.get_request_header('X-Forwarded-For'):
            frappe.local.request_ip = (frappe.get_request_header('X-Forwarded-For').split(",")[0]).strip()

        elif frappe.get_request_header('REMOTE_ADDR'):
            frappe.local.request_ip = frappe.get_request_header('REMOTE_ADDR')

        else:
            frappe.local.request_ip = '127.0.0.1'

        # language
        self.set_lang()

        # load cookies
        frappe.local.cookie_manager = CookieManager()

        # set # db
        self.connect()

        # login
        frappe.local.login_manager = LoginManager()

        if frappe.form_dict._lang:
            lang = get_lang_code(frappe.form_dict._lang)
            if lang:
                frappe.local.lang = lang

        self.validate_csrf_token()

        # write out latest cookies
        frappe.local.cookie_manager.init_cookies()

        # check status
        check_session_stopped()


class LoginManager(FrappeLoginManager):

    def post_login(self):
        self.run_trigger('on_login')
        validate_ip_address(self.user)
        self.validate_hour()
        self.get_user_info()
        self.make_session()
        self.set_user_info()
        self.set_redirect_url()

    def set_redirect_url(self):
        redirect_to = get_login_redirect()
        frappe.local.response["redirect_to"] = redirect_to
        frappe.local.response["home_page"] =  redirect_to
