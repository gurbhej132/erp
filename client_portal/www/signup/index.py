'''
'''
import frappe
from frappe import _
import json
from six import string_types
from frappe.utils.password import update_password
from client_portal.utils.password import test_password_strength

no_cache=True
def get_context(context):

    context.update({
        "show_background_image": True
    })


@frappe.whitelist(allow_guest=True)
def signup(args):
    if(isinstance(args, string_types)):
        args = json.loads(args)

    error = False
    error_message = ""
    args = frappe._dict(args)
    if(not args.get("first_name")):
        error = True
        error_message = _("Please enter first name")
    if(not args.get("last_name")):
        error = True
        error_message = _("Please enter last name")
    if(not args.get("email")):
        error_message = _("Please enter email")
    if(not args.get("password")):
        error = True
        error_message = _("Please enter password")
    if(not args.get("accept_terms")):
        error = True
        error_message = _("Please accept terms")

    pwd_msg = test_password_strength(args.get("password"), args.get("confirmpassword"))
    if not(pwd_msg.get("flag")):
        error = True
        error_message = pwd_msg.get("error_message")

    user = None

    if(frappe.db.get_value("User", {"name": args.get("email")})):
        error = True
        error_message = _("User already exists")

    if(not error):
        args.update({
            "doctype": "User",
            "send_welcome_email": 0,
        })
        user=None
        try:
            user = frappe.get_doc(args)
            user.save(ignore_permissions=True)
            update_password(user.name, args.get("password"), logout_all_sessions=True)
            add_user_permissions(user)

        except Exception as e:
            error = True
            error_message = _("Error while processing signup")

    return {
        "error": error,
        "error_message": error_message,
        "user": user
    }

def setup_user_permissions(user):
    # add user permission
    from client_portal.utils.permissions import setup_permissions
    setup_permissions(user)
