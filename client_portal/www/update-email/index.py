'''
'''


from __future__ import unicode_literals

import frappe
from frappe import _
from frappe.utils import today, nowdate, now_datetime, add_days

from frappe.model.rename_doc import rename_doc
no_cache = 1


def get_context(context):
	form = frappe.local.form_dict
	context.update({
		"failed": True,
		"email_update_message": _("Someting went wrong")
	})
	if(not form.get("key") and frappe.session.user == "Guest"):
		context.update({
			"failed": True,
			"email_update_message": _("Email address updated successful")
		})
		return

	key = form.get("key")
	email_update_request = frappe.db.get_value("Email Update Request", {"email_key": key},
												["name", "validity", "status", "new_email", "last_email"], as_dict=True)
	if(not email_update_request or
		(email_update_request and email_update_request.get("status") != "Pending") or
		(email_update_request and email_update_request.get("validity") < now_datetime())):
		context.update({
			"failed": True,
			"email_update_message": _("Link has been expired/used already"),
		})
		return

	# Doctype, lastemail, new email
	frappe.db.sql("""DELETE from `tabNotification Settings` WHERE name ='%s' """%(email_update_request.get("last_email")))


	rename_doc("User", email_update_request.get("last_email"),
						email_update_request.get("new_email"))

	frappe.db.set_value("Email Update Request", email_update_request.get("name"), "status", "Updated")

	redirect_url = "/login"
	if(frappe.session.user != "Guest"):
		redirect_url = "/dashboard"

	context.update({
		"email_update_message": _("Email updated successfully"),
		"redirect_url": redirect_url
	})

	frappe.db.commit()
	return
