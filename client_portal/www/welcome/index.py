'''
'''

import frappe
from frappe import _

no_cache = True

def get_context(context):
    context.update({
        "hide_side_bar": True
    })
