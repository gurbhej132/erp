'''
'''

import frappe

no_cache=True

def get_context(context):
    form_dict = frappe.local.form_dict
    if(form_dict.get("key")):
        user = frappe.db.get_value("User", {"reset_password_key": form_dict.get("key")})
        if not user:
            context.update({
                "invalid_user_key": True
            })
            return
        else:
            frappe.db.set_value("User", user, "reset_password_key", "")
            frappe.db.set_value("User", user, "enabled", 0)
            context.update({
                "email_id": user
            })
            frappe.db.commit()
    else:
        context.update({
            "invalid_user_key": True
        })
