'''
'''

import frappe
from frappe import _
from frappe.utils import get_formatted_email

no_cache=True

STANDARD_USERS = ("Guest", "Administrator")

def get_context(context):
    context.update({
        "show_background_image": True
    })

@frappe.whitelist(allow_guest=True)
def reset_password(user):
    msg = frappe._dict({"error": False, "error_message": ""})
    if user=="Administrator":
        msg.error = True
        msg.error_message = "not allowed"
        return msg

    try:
        user = frappe.get_doc("User", user)
        if not user.enabled:
            msg.error = True
            msg.error_message = 'user is disabled'
            return msg

        user.validate_reset_password()
        _reset_password(user, send_email=True)
        return msg

    except frappe.DoesNotExistError:
        frappe.clear_messages()
        msg.error = True
        msg.error_message = 'The email <b>%s</b> is not yet used for an account '%(user)
        return msg
    except Exception as e:
        msg.error = True
        msg.error_message = "something went wrong"
        return msg



def _reset_password(user, send_email=False, password_expired=False, now=True):
    from frappe.utils import random_string, get_url
    from frappe.utils.user import get_user_fullname

    key = random_string(32)

    user.db_set("reset_password_key", key)

    url = "/update-password?key=" + key
    disable_user_url = "/disable-user?key=" + key

    if password_expired:
    	url = "/update-password?key=" + key + '&password_expired=true'

    link = get_url(url)
    # Disabled user, if user didn't raise request to reset the password.

    disable_user_link = get_url(disable_user_url)

    if send_email:
        subject = _("Password Reset")
        template = "password_reset"

        full_name = get_user_fullname(frappe.session['user'])
        if full_name == "Guest":
        	full_name = "Administrator"

        args = {
        	'first_name': user.first_name or user.last_name or "user",
        	'user': user.name,
        	'title': subject,
        	'login_url': get_url(),
        	'user_fullname': full_name,
            "link": link,
            "disable_user_link": disable_user_link
        }

        sender = frappe.session.user not in STANDARD_USERS and get_formatted_email(frappe.session.user) or None

        frappe.sendmail(recipients=user.email, sender=sender, subject=subject,
        	template=template, args=args, header=[subject, "green"],
        	delayed=(not now) if now!=None else self.flags.delay_emails, retry=3)

    return link
