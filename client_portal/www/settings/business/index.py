'''

'''

import frappe
from frappe import _

no_cache=True

def get_context(context):
    pass



def get_form(route, method):
    user = frappe.session.user
    doc = frappe.get_doc("DocType", "Business Information")

    existing_business = get_existing_businesses(user)

    return {
        "form_title": "Business Information",
        "fields": doc.fields,
        "as_grid": True,
        "values": existing_business,
        "allow_update": True,
        "save_button": "Save"
    }


def save_form(form, route, method):

    user = frappe.session.user
    doc = None
    if(form.get("legal_name") and frappe.db.exists("Business Information", form.get("legal_name"))):
        doc = frappe.get_doc("Business Information", form.get("legal_name"))
    else:
        doc = frappe.new_doc("Business Information")

    doc.update(form)

    # Business User entity map, already linked
    existing_business_user_entity_map = frappe.db.get_value("Business User Entity",
                {"user": user, "business_information": form.get("business_information")})


    doc.save(ignore_permissions=True)

    business_user_entity = None
    if(existing_business_user_entity_map):
        business_user_entity = frappe.get_doc("Business User Entity", existing_business_user_entity_map)
    else:
        business_user_entity = frappe.new_doc("Business User Entity")

    business_user_entity.update({
        "user": user, "business_information": doc.name,
    })
    business_user_entity.save(ignore_permissions=True)

    frappe.db.commit()
    return {
        "title": _("Updated"),
        "doc": doc
    }


'''
    Get all existing user business information
'''
def get_existing_businesses(user):
    existing_businesses =  {}
    user_business_entities = ", ".join(["'%s'"%(ub.business_information) for ub in
                                frappe.db.sql("""SELECT `tabBusiness User Entity`.user,
                                `tabBusiness User Entity`.business_information FROM `tabBusiness User Entity`
                                WHERE `tabBusiness User Entity`.user = '%s'"""%(user), as_dict=True)])
    if(not user_business_entities):
        return existing_businesses

    return frappe.db.sql("""SELECT `tabBusiness Information`.* FROM `tabBusiness Information`
            WHERE `tabBusiness Information`.name IN (%s) """%(user_business_entities), as_dict=True)[0]
