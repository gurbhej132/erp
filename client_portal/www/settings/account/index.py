'''
'''

import frappe
from frappe import _
from frappe.utils import now_datetime


no_cache=True

def get_context(context):
    pass


def get_user_form(route, method):
    user = frappe.get_doc("User", frappe.session.user)
    home_address = None
    mailing_address = None

    user_address = frappe.db.get_value("User Address", {"user": user.name}, ["home_address", "mailing_address"], as_dict=True)
    if(user_address):
        home_address = user_address.home_address
        mailing_address = user_address.mailing_address

    fields = [{
            "fieldname": "first_name", "fieldtype": "Data", "default": user.first_name, "reqd": 1,
            "only_input":True, "label": _("First Name")
        },{
            "fieldtype": "Column Break",
        },{
            "fieldname": "last_name", "fieldtype": "Data", "default": user.last_name, "reqd":1,
            "only_input": True, "label": _("Last Name")
        },{
            "fieldtype": "Section Break",
        },{
            "fieldname": "phone", "fieldtype": "Data", "options": "Phone", "label": _("Phone"),
            "default": user.mobile_no
        },{
            "fieldname": "home_address", "fieldtype": "Address", "label":_("Home Address"), "value": home_address
        },{
            "fieldname": "mailing_address", "fieldtype": "Address", "label": _("Mailing Address"), "value": mailing_address
        },{
            "fieldname": "dob", "fieldtype": "Date", "label":_("DOB"), "default": user.birth_date
        },{
            "fieldname": "marital_status", "fieldtype": "Select", "label": _("Marital Status"),
            "options": ['Male', 'Female'], "default": "Male"
        }]

    form = frappe._dict({
        "form_title": "Personal Information",
        "fields": fields,
        "allow_update": True,
        "save_button": "Save"
    })

    return form


'''
    Save User Personal Information
'''
def save_user_form(form, route, method):
    from client_portal.bond_portal.doctype.user_address.user_address import make_user_address
    user =  frappe.get_doc("User", frappe.session.user)

    user_address = make_user_address(user.name, form or {})

    user.update({
        "first_name": form.get("first_name"),
        "last_name": form.get("last_name"),
        "marital_status": form.get("marital_status"),
        "birth_date": form.get("dob"),
        "mobile_no": form.get("phone")
    })
    user.save(ignore_permissions=True)

    return form

'''
    Get Email Form
'''
def get_email_form(route, method):
    user = frappe.get_doc("User", frappe.session.user)
    form_title = "Emails"
    description = None
    new_email_request = frappe.db.get_value("Email Update Request",
            {"last_email": user.name, "status": "Pending", "validity": [">", now_datetime()]}, "new_email",
            order_by='validity DESC')

    if(new_email_request):
        description = "%s email"%(new_email_request)

    fields = [{
        "fieldname": "email", "fieldtype": "Data", "options": "Email", "label": _("Email"),
        "default": user.email, "description": description
    }]
    return frappe._dict({
        "form_title": form_title,
        "fields": fields,
        "allow_update": True,
        "save_button": "Save"
    })

'''
    Save Eamil Form
'''
def save_email_form(form, route, method):
    from client_portal.utils.email import update_email

    if not(form.get("email")):
        frappe.throw(_("Please enter email"));

    return update_email(form.get("email"))
