'''
'''

import frappe
from frappe import _
import json
from six import string_types

no_cache = True

def get_context(context):

    if(frappe.session.user != "Guest"):
        user = frappe.get_doc("User", frappe.session.user)
        context.update({
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email
        })

@frappe.whitelist(allow_guest=True)
def contactus(args):
    if(isinstance(args, string_types)):
        args  = json.loads(args)

    args.update({
        "doctype": "Bond Contact Us",
        "send_email": True
    })
    frappe.get_doc(args).save(ignore_permissions=True)
