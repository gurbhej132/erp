
from __future__ import unicode_literals

import frappe
from frappe import _
from frappe.utils import today
from frappe.utils.password import update_password as _update_password
from frappe.core.doctype.user.user import (_get_user_for_update_password,
			handle_password_test_fail, reset_user_data)

from bondportal.utils.password import test_password_strength as _test_password_strength
import re

no_cache = 1

def get_context(context):
	form = frappe.local.form_dict
	if(not form.get("key") and frappe.session.user == "Guest"):
		context.update({
			"invalid_password_link": True
		})
	if(not form.get("key")):
		context.update({
			"invalid_password_link": True
		})
	context.show_background_image = True

@frappe.whitelist(allow_guest=True)
def update_password(new_password, confirm_password, logout_all_sessions=0, key=None, old_password=None):


	msg = _test_password_strength(new_password)

	if not msg.get("flag"):
		frappe.local.response.http_status_code = 410
		return msg.get("error_message")

	if(new_password != confirm_password):
		frappe.local.response.http_status_code = 410
		return _("The password and confirm password does not match")



	res = _get_user_for_update_password(key, old_password)

	if res.get('message'):
		frappe.local.response.http_status_code = 410
		return res['message']
	else:
		user = res['user']


	_update_password(user, new_password, logout_all_sessions=int(logout_all_sessions))

	user_doc, redirect_url = reset_user_data(user)

	# get redirect url from cache
	redirect_to = "/dashboard"

	frappe.local.login_manager.login_as(user)

	frappe.db.set_value("User", user, "last_password_reset_date", today())
	frappe.db.set_value("User", user, "reset_password_key", "")

	if user_doc.user_type == "System User":
		return "/dashboard"
	else:
		return redirect_url if redirect_url else "/dashboard"




@frappe.whitelist(allow_guest=True)
def test_password_strength(password, confirmpassword=False):
	return _test_password_strength(password, confirmpassword)
