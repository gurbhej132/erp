'''
    Base Controller for Website Insurance Form(Personal Auto)
'''

import frappe

no_cache = True

def get_context(context):
    from client_portal.insurances_form.insurance_form import get_form_meta
    context.update({
        "form_data": frappe.as_json(get_form_meta("Personal Auto"))
    })
