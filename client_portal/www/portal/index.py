'''
'''

import frappe
from frappe import _

def get_context(context):
    context.update({
        "hide_login": True,
        "show_background_image": True
    })


@frappe.whitelist()
def get_dashboard_options():
    return [{
        "label": _("Apply For"),
        "items":[{
                "label": _("Business Financing"),
                "link": "",
            },{
                "label": _("Business Insurance"),
                "link": "",
                "has_sub_options": True,
                "sub_options":[{
                    "label": _("Business Insurance"),
                    "items":[{
                        "label": "Item 1",
                        "link": "",
                    },{
                        "label": "Item 2",
                        "link": ""
                    },{
                        "label": "Item 3",
                        "link": "#"
                    }],
                }]
        },{
            "label": _("Payroll & Human Resources"),
            "link": "#"
        },{
            "label": _("Payment Processing"),
            "link": "#"
        },{
            "label": _("Platform"),
            "link": "#"
        }]},{
                "label": _("Your Services"),
                "items":[{
                        "label": _("Item 1"),
                        "link": "",
                    },{
                        "label": _("Item 1"),
                        "link": "",
                    },{
                        "label": _("Item 1"),
                        "link": "",
                }],
        },{
            "label": _("Settings"),
            "items":[{
                "label": _("Personal"),
                "link": "/settings/account"
            },{
                "label": _("Business"),
                "link": "/settings/business"
            }]
        }]
