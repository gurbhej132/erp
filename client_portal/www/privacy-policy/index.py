'''
'''

import frappe
from frappe import _

no_cache=True
def get_context(context):
    settings = frappe.get_doc("Bond Portal Settings", "Bond Portal Settings")
    if(settings.privacy_policy):
        context.update({
            "privacy_policy": frappe.get_doc("Privacy Policy", settings.privacy_policy).as_dict(),
            "show_background_image": False
        })
