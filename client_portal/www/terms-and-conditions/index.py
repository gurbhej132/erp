'''
'''

import frappe
from frappe import _

no_cache=True

def get_context(context):
    settings = frappe.get_doc("Bond Portal Settings", "Bond Portal Settings")
    if(settings.default_terms_and_conditions):
        context.update({
            "terms_and_conditions": frappe.get_doc("T and C", settings.default_terms_and_conditions).as_dict(),
            "show_background_image": False
        })
