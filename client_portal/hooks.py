# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "client_portal"
app_title = "Client Portal"
app_publisher = "Client Portal"
app_description = "Client Portal"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "navdeep@bondnorthamerica.com"
app_license = "Apache"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/client_portal/css/client_portal.css"
app_include_js = [
    "/assets/js/client-portal-app.min.js"
]

# include js, css files in header of web template
web_include_css = [
    "/assets/css/client-portal-web-min.css"
]
web_include_js = [
]

fixtures = [{
    "dt": "Custom Field", "filters":[["dt", "in", ["Address"]]]

}]
# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
#home_page = ""

update_website_context = "client_portal.website.website.update_website_context"

# Update boot for logged in user
boot_session = "client_portal.boot.update_boot_context"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "client_portal.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "client_portal.install.before_install"
# after_install = "client_portal.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "client_portal.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"client_portal.tasks.all"
# 	],
# 	"daily": [
# 		"client_portal.tasks.daily"
# 	],
# 	"hourly": [
# 		"client_portal.tasks.hourly"
# 	],
# 	"weekly": [
# 		"client_portal.tasks.weekly"
# 	]
# 	"monthly": [
# 		"client_portal.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "client_portal.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "client_portal.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "client_portal.task.get_dashboard_data"
# }

on_login     = "client_portal.login.on_after_login"
