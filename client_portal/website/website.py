'''
'''

import frappe
from frappe import _
import json
from frappe.boot import add_timezone_info, get_user

def update_website_context(context):

    is_method = frappe.local.request.path.find("method")

    '''
    if(frappe.local.conf.is_password_protected == True and frappe.session.user == "Guest"
            and frappe.local.request.path not in ["/login", '/'] and is_method < 0):
        frappe.local.flags.redirect_location = "/login"
        raise frappe.Redirect

    paths = frappe.local.request.path.split("/")

    if(frappe.session.user == "Guest"):
        if('blogs' in paths):
            pass
        elif('connect' in paths):
            pass
        elif(frappe.local.request.path not in ['/signup', '/', '/login', '/forgot-password',
                        '/disable-user', '/update-password', '/terms-and-conditions', '/privacy-policy',
                        '/feedback', '/contactus', '/update-email', '/website_script'] and is_method < 0):
            frappe.local.flags.redirect_location = "/login"
            raise frappe.Redirect
    '''
    
    context.update({
        "user": frappe.get_doc("User", frappe.session.user).as_dict(),
    })
    _update_website_context(context)

    context.update({
        "context": context,
    })

def _update_website_context(context):
    # Update all the default context values
    update_meta_tags(context)
    boot_info = frappe._dict()
    get_user(boot_info)
    boot_info.sysdefaults = frappe.defaults.get_defaults()
    add_timezone_info(boot_info)
    boot_info.update({
        "client_portal": context.get("user"),
    })

    context.update({
        "hide_login": True,
        "boot":frappe.as_json(boot_info)
    })

def update_meta_tags(context):
    items = {}
    for web_meta in frappe.db.sql("""SELECT name FROM `tabWebsite Route Meta`  WHERE name = '/index' """, as_dict=True):
        metatag = frappe.get_doc("Website Route Meta", web_meta.name)
        for tag in metatag.meta_tags:
            tag.set_in_context(context)
    return items
