'''

'''
import frappe

'''
    Get sub route if it's available
'''
def is_sub_route(website_path):
    website_routes = get_render_website_routes()
    web_paths = website_path.split("/")
    if not website_routes:
        return

    path = web_paths[-0]

    page = None
    if(path in website_routes):
        page = website_routes.get(path)
        page.update({
            "name":web_paths[-1]
        })

    route = None
    if(page):
        route = "/".join(web_paths[0:-1])
        if(route):
            frappe.local.form_dict.update({
                "route_controller": page
            })
    return route

'''
    Convert all hooks route to dict
'''
def get_render_website_routes():
    routes = frappe._dict()
    website_routes = frappe.get_hooks("render_website_routes")

    for route in website_routes:
        routes[route.get("route_starts_width")] = route
    return routes
