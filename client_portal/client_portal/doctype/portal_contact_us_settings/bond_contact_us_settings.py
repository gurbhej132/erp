# -*- coding: utf-8 -*-
# Copyright (c) 2020, Client Portal and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PortalContactUsSettings(Document):
	pass

def get_emails():
	return [e.email for e in frappe.get_single("Portal Contact Us Settings").emails]
