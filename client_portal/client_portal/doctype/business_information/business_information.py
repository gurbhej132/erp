# -*- coding: utf-8 -*-
# Copyright (c) 2020, Client Portal and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import cstr
class BusinessInformation(Document):
	pass


'''
	Create business entry from lead
'''
def make_business_entry(lead):
	business_info = None
	legal_name = cstr(lead.business_legal_name).strip()
	if not(legal_name):
		legal_name = cstr(lead.business_dba).strip()

	if(frappe.db.get_value("Business Information", legal_name)):
		business_info = frappe.get_doc("Business Information", legal_name)
	else:
		business_info = frappe.new_doc("Business Information")
		business_info.legal_name = legal_name

	business_info.update({
		"dba": lead.business_dba,
		"business_address_line1": lead.business_address_line1,
		"business_address_line2": lead.business_address_line2,
		"business_city": lead.business_city, "business_state": lead.business_state,
		"business_zip_code": lead.business_zip_code,
		"mailing_address_line1": lead.mailing_address_line1,
		"mailing_address_line2": lead.mailing_address_line2,
		"mailing_city": lead.mailing_city, "mailing_state": lead.mailing_state,
		"mailing_zip_code": lead.mailing_zip_code,
		"business_phone_number": lead.business_phone_number,
		"business_email": lead.business_email,
		"business_website": lead.website,
		"from_lead": lead.name
	})

	business_info.save()
