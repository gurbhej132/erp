# -*- coding: utf-8 -*-
# Copyright (c) 2020, Client Portal and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class UserAddress(Document):
	pass

# Make user address
def make_user_address(user, form):
	user_address = frappe.db.get_value("User Address", user)

	# If user address doesn't exists
	if(not user_address):
		# create new user address
		user_address = frappe.get_doc({"doctype": "User Address", "user": user})
		user_address.save(ignore_permissions=True)
	else:
		# get existing user address
		user_address = frappe.get_doc("User Address", user)

	# Create and save address
	if(form.get("home_address")):
		user_address.home_address = make_or_update_address(user, user_address.home_address,
											"Home Address", form.get("home_address"))
	if(form.get("mailing_address")):
		user_address.mailing_address = make_or_update_address(user, user_address.mailing_address,
										"Mailing Address", form.get("mailing_address"))

	# Save the changes to the database
	user_address.save(ignore_permissions=True)

	return user_address


# Create or update the existing address linked to the user
def make_or_update_address(user, address, address_type, values={}):
	doc = None
	if(not values):
		return doc

	if(address and frappe.db.get_value("Address", address)):
		doc = frappe.get_doc("Address", address)

	elif(not address and values):
		doc = frappe.new_doc("Address")

	# Update the doc with values
	doc.update({
		"doctype": "Address", "address_type": address_type,
		"address_line1": values.get("street_name"),
		"address_line2": values.get("apartment_no"),
		"city": values.get("city"), "state": values.get("state"),
		"pincode": values.get("zip_code"), "address_title": user
	})
	if(not hasattr(doc, "links")):
		doc.links = []

	flag = False
	for val in doc.links:
		if(val.link_doctype == "User Address" and val.link_name == user):
			flag = True

	if(not flag):
		doc.append("links", {
			"link_doctype": "User Address",
			"link_name": user,
		})

	doc.save(ignore_permissions=True)

	return doc.name
