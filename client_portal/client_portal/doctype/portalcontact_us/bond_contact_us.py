# -*- coding: utf-8 -*-
# Copyright (c) 2020, Client Portal and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from client_portal.client_portal.doctype.portal_contact_us_settings.portal_contact_us_settings import get_emails

class PortalContactUs(Document):
	def validate(self):
		self.validate_feedback_info()
		self.send_feedback_email()

	def validate_feedback_info(self):
		self.full_name = self.first_name + " " + self.last_name

	def send_feedback_email(self):

		if(self.send_email and not self.email_sent):
			emails = get_emails()
			message = frappe.render_template("client_portal/templates/emails/contactus.html", self.as_dict())
			frappe.sendmail(recipients=emails, message=message, subject="Portal Contact", delayed=False,
							reference_doctype=self.meta.name, reference_name=self.name)
			self.email_sent = True
