# -*- coding: utf-8 -*-
# Copyright (c) 2021, Client Portal and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.naming import make_autoname
from frappe.model.document import Document
from six import string_types
import json
from frappe import _
from frappe.modules import scrub, get_module_path, load_doctype_module
import os
import pathlib


NON_STANDARD_FIELDS = ['Section Break', 'Column Break', 'HTML', 'Table', 'Signature']

class WebsiteInsuranceForm(Document):

	def validate(self):
		self.flags.ignore_mandatory = True
		self.validate_duplicate_fields()
		self.validate_fields()
		self.update_fields()
		self.generate_module_and_files()

	def update_fields(self):
		if(not self.form_fields):
			self.update_main_form_fields()
			self.update_child_form_fields()

	def update_main_form_fields(self):
		data = get_fields(self.reference_type)
		if(data and data.get("fields_map")):
			for fieldname, field in data.get("fields_map", {}).items():
				self.append("form_fields", {
					"fieldname": field.get("fieldname"),
					"fieldtype": field.get("fieldtype"),
					"label": field.get("label"),
					"options": field.get("options"),
				})


	def update_child_form_fields(self):
		child_tables = get_child_tables(self.reference_type)
		fields = child_tables.get("fields")
		fields_map = child_tables.get("fields_map")

		for table in fields:
			data = get_fields(table.get("label"))
			if(data and data.get("fields_map")):
				for fieldname, field in data.get("fields_map").items():
					self.append("form_fields", {
						"fieldname": field.get("fieldname"),
						"fieldtype": field.get("fieldtype"),
						"label": field.get("label"),
						"is_table_field": True,
						"options": field.get("options"),
						"table_fieldname": table.get("value"),
						"table_reference_type": table.get("label")
					})

	def validate_duplicate_fields(self):
		self.validate_parent_form_duplicate_fields()
		self.validate_child_form_duplicate_fields()

	def validate_parent_form_duplicate_fields(self):

		fields = []
		for field in self.form_fields:
			if(not field.is_table_field):
				if(field.fieldname in fields):
					frappe.throw(_("Duplicate fieldname <b>%s</b>"%(field.label)))
				fields.append(field.fieldname)

	def validate_child_form_duplicate_fields(self):

		fields = []
		for field in self.form_fields:

			if(field.is_table_field):
				label = "%s-%s"%(field.fieldname, field.table_reference_type)
				if(label in fields):
					frappe.throw(_("Duplicate for table: <b>%s</b> and field <b>%s</b> at row #%s"%(
						field.table_reference_type, field.label, field.idx)))
				fields.append(label)


	def validate_fields(self):
		for field in self.form_fields:
			if(field.fieldtype in ["Section Break", "Column Break"]):
				field.fieldname = "%s_%s"%(frappe.scrub(field.fieldtype), field.idx)

			if(not self.steps):
				if(field.fieldtype == "Section Break"):
					self.append("steps", {
						"label": field.label,
						"button_name": field.label,
						"section_fieldname": field.fieldname
					})



	def generate_module_and_files(self):
		self.module = "Insurances Form"
		doctype_name  = frappe.scrub(self.name)
		module_path = os.path.join(get_module_path(self.module), "forms", doctype_name)
		js_file_path = os.path.join(module_path, "%s.js"%(doctype_name))
		py_file_path = os.path.join(module_path, "%s.py"%(doctype_name))
		module_file_path = os.path.join(module_path, "__init__.py")

		if(not(os.path.exists(module_path))):
			pathlib.Path(module_path).mkdir(parents=True, exist_ok=True)

		if(not(os.path.exists(js_file_path))):
			open(js_file_path, "a").close()

		if(not(os.path.exists(py_file_path))):
			open(py_file_path, "a").close()

		if(not(os.path.exists(module_file_path))):
			open(module_file_path, "a").close()




'''
	Get all custom fields
'''
@frappe.whitelist()
def get_child_tables(reference_type):

	doc = frappe.get_doc("DocType", reference_type)
	table_fields = [f.as_dict() for f in doc.fields if f.fieldtype == "Table"]
	custom_fields = frappe.db.get_values("Custom Field", {
									"fieldtype": "Table",
									"dt": reference_type,
									}, "options", as_dict=True)

	fields = [{"value": f.fieldname, "label": f.options} for f in table_fields if f.fieldtype == "Table"]
	fields.extend([{"value": cf.fieldname, "label": cf.options} for cf in custom_fields])
	fields_map = {}
	table_fields.extend(custom_fields)

	for f in table_fields:
		if(f.get("fieldtype") == "Table"):
			fields_map.setdefault(f.fieldname, f)

	return {
		"fields": fields,
		"fields_map": fields_map
	}

'''
	Get Fields
'''
@frappe.whitelist()
def get_fields(reference_type):

	doc = frappe.get_doc("DocType", reference_type)
	ignore_fields = ", ".join(["'%s'"%(f) for f in NON_STANDARD_FIELDS])
	condition = " AND dt='%s' AND fieldtype NOT IN (%s)"%(reference_type, ignore_fields)

	fields = [f.as_dict() for f in doc.fields if f.fieldtype not in NON_STANDARD_FIELDS]
	fields.extend(frappe.db.sql("""SELECT * FROM `tabCustom Field`
							WHERE docstatus != 2 %s """%(condition), as_dict=True))

	options = []
	fields_map = {}
	for f in fields:
		options.append({"value": f.get("fieldname"), "label": f.get("label")})
		fields_map.setdefault(f.get("fieldname"), f)

	return frappe._dict(
			fields_map = fields_map,
			options = options)


'''
	Sync template with doc
'''
@frappe.whitelist()
def sync_template_with_doc(doc, template):

	if(isinstance(doc, string_types)):
		doc = json.loads(doc)

	template = frappe.get_doc("Document Template", template)

	fields_map = frappe._dict()
	for field in template.fields:
		if(field.is_table):
			fields_map.setdefault(field.table_fieldname, frappe._dict())
			fields_map[field.table_fieldname].setdefault(field.fieldname, field.field_value)
		else:
			fields_map.setdefault(field.fieldname, field.field_value)


	for key, val in fields_map.items():
		doc[key] = val

	return doc
