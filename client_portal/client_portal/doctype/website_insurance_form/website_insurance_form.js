// Copyright (c) 2021, Client Portal and contributors
// For license information, please see license.txt


client_portal.controllers.WebsiteInsuranceForm = Class.extend({
    init: function(args){
        $.extend(this, args);
    },

    setup: function(){
        this.frm.set_query("reference_type", (doc, cdt, cdn)=>{
            return {
                filters:{
                    issingle: false,
                    istable: false,
                    module: "Insurance Services",
                    name: this.frm.doctype,
                    name:["NOT IN", ["`tabWebsite Insurance Form`.name"]]
                }
            }
        });
        if(!this.frm.is_new()){
            this.frm.set_df_property("reference_type", "read_only", true);
        }

        this.frm.get_field("form_fields").grid.editable_fields = [
            {fieldname: "label"},
            {fieldname: "fieldtype"},
            {fieldname: "is_table_field",},
            {fieldname: "table_reference_type"},
            {fieldname: "end_sub_form"},
        ]
    },

    refresh: function(){

        this.frm.add_custom_button(__("Add Insurance Child Form Field"), ()=>{
            this.add_table_document_field();
        });

        this.frm.add_custom_button(__("Add Insurance Form Fields"), ()=>{
            this.add_document_field();
        });
    },

    add_document_field: function(){
        if(!this.validate_mandatory()){
            return false;
        }

        let dialog = new frappe.ui.Dialog({
            title: __("Add New Field for Table"),
            fields:[{
                    fieldname: "reference_type", fieldtype: "Select", options: [this.frm.doc.reference_type],
                    read_only: true, reqd:1, label: __("Document Type"), default: this.frm.doc.reference_type
                },{
                    fieldname: "fieldname", fieldtype: "Select", options: [],
                    reqd:1, label: __("Select Field")
            }],
            primary_action: (doc)=>{
                this.add_field(doc, false);
            },
            primary_action_label: __("Add Field")
        });

        this.get_and_update_fields(dialog, dialog.get_value("reference_type"), "fieldname");

        dialog.show();

    },

    add_table_document_field: function(){
        if(!this.validate_mandatory()){
            return false;
        }
        let me = this;
        frappe.call({
            method: "client_portal.client_portal.doctype.website_insurance_form.website_insurance_form.get_child_tables",
            args:{
                reference_type: this.frm.doc.reference_type,
            },
            freeze: true,
            callback: function(res){
                if(res && res.message){
                    let dialog = new frappe.ui.Dialog({
                        title: __("Add New Field For Child Table"),
                        fields:[{
                            fieldname: "table_reference_type", fieldtype: "Select", options: res.message.fields,
                            reqd:1, label: __("Table Document Type")
                        },{
                            fieldname: "table_fieldname", fieldtype: "Select", options: [],
                            reqd:1, label: __("Select Table Field")
                        }],
                        primary_action: function(doc){
                            let args = {
                                fieldname: doc.table_fieldname
                            }
                            me.add_field(args, true, res.message.fields_map[doc.table_reference_type]);
                        },
                        primary_action_label: __("Add Table Field")
                    });

                    dialog.fields_dict.table_reference_type.$input.on("change", function(e){
                        if(!dialog.get_value("table_reference_type")){
                            frappe.msgprint(__("Please select Table Document Type"));
                            return false;
                        }
                        let table_reference_type = res.message.fields_map[dialog.get_value("table_reference_type")].options;
                        me.get_and_update_fields(dialog, table_reference_type, "table_fieldname");
                    });

                    dialog.show();
                }
            }
        });
    },

    validate_mandatory: function(){
        if(!(this.frm.doc.reference_type)){
            frappe.msgprint(__("Please select Document Type in main form"));
            return false;
        }
        return true;
    },

    get_and_update_fields: function(dialog, reference_type, fieldname){

        frappe.call({
            method: "client_portal.client_portal.doctype.website_insurance_form.website_insurance_form.get_fields",
            args:{
                reference_type: reference_type,
            },
            freeze: true,
            callback: (res)=>{
                this.res = null;
                if(res && res.message){
                    this.res = res.message;
                    dialog.set_df_property(fieldname, "options", [""].concat(res.message.options));
                }
            }
        });
    },

    add_field: function(doc, is_child, table_field_info){
        if(this.res){
            let field_info = this.res.fields_map[doc.fieldname];

            this.frm.add_child("form_fields",{
                fieldtype: field_info.fieldtype,
                fieldname: field_info.fieldname,
                label: field_info.label,
                is_table_field: is_child?true:false,
                options: field_info.options,
                table_fieldname: is_child?table_field_info.fieldname:null,
                table_reference_type: is_child?table_field_info.options:""
            });
            this.frm.refresh_field("form_fields");
        }
    }
});

cur_frm.script_manager.make(client_portal.controllers.WebsiteInsuranceForm);
