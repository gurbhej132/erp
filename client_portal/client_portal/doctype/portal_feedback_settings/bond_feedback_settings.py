# -*- coding: utf-8 -*-
# Copyright (c) 2020, Portal Portal and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PortalFeedbackSettings(Document):
	pass


def get_emails():
	return [e.email for e in frappe.get_single("Portal Feedback Settings").emails]
