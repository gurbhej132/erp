'''
    Base Controller for Client Portal and Portal Blogs
'''

import frappe

class BaseController(object):

    def __init__(self, doc, doctype, method):
        self.doctype = doctype
        self.method = method
        self.doc = doc


    def validate(self):
        pass

    def on_submit(self):
        pass

    def on_cancel(self):
        pass
