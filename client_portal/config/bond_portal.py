'''
'''

import frappe
from frappe import _


def get_data():

    return [{
        "label": _("Business"),
        "items":[{
            "type": "doctype",
            "name": "Business Information",
            "icon": "",
            "description": _("View/Edit Business Information"),
            "label": _("Business Information")
        },{
            "type": "doctype",
            "name": "Business User Entity",
            "description": _("View/Edit Business User Entity"),
            "label": _("Business User Entity")
        }]
    },{
        "label": _("Account"),
        "items":[{
            "type": "doctype",
            "name": "User",
            "icon": "",
            "description": _("View/Edit User Information"),
            "label": _("User Account")
        },{
            "type": "doctype",
            "name": "Email Update Request",
            "description": "",
            "label": _("Email Update Request")
        }]
    },{
        "label": _("Contact"),
        "items":[{
            "type": "doctype",
            "name": "Portal Contact Us Settings",
            "label": _("Portal Contact Us Settings"),
            "description": _("Portal Contact Us Settings"),
        },{
            "type": "doctype",
            "name": "Portal Contact Us",
            "label": _("Portal Contact Us"),
            "description": _("View/Create contacts")
        }]
    },{
        "label": _("Feedback"),
        "items": [{
            "type": "doctype",
            "name": "Portal Feedback Settings",
            "label": _("Portal Feedback Settings"),
            "description": _("Portal Feedback Settings"),
        },{
            "type": "doctype",
            "name": "Portal Feedback",
            "label": _("Portal Feedback"),
            "description": _("Portal Feedback")
        }]
    },{
        "label": _("Address"),
        "items":[{
            "type": "doctype",
            "name": "User Address",
            "label": _("User Address")
        },{
            "type": "doctype",
            "name": "State",
            "label": _("State")
        },{
            "type": "doctype",
            "name": "Zip Code",
            "label": _("Zip Code")
        },{
            "type": "doctype",
            "name": "City",
            "label": _("City")
        }]
    },{
        "label": _("Privacy Policy and T&C"),
        "items":[{
            "type": "doctype",
            "name": "Privacy Policy",
            "label": _("Privacy Policy")
        },{
            "type": "doctype",
            "name": "T and C",
            "label": _("Terms and Conditions")
        }]
    },{
        "label": _("Settings"),
        "items": [{
            "type": "doctype",
            "name": "Client Portal Settings",
            "label": "Client Portal Settings"
        }]
    }]
