'''
    Base Controller for Website Insurance Form
'''

import frappe
from frappe import _dict
import os
from frappe.modules import scrub, get_module_path, load_doctype_module
from frappe.model.utils import render_include
from client_portal.client_portal.doctype.website_insurance_form.website_insurance_form import get_fields



@frappe.whitelist(allow_guest=True)
def get_form_meta(form_type):
    return FormMeta(form_type)

class Meta(_dict):

    def set(self, key, value, as_value=False):
        self.update({
            key: value
        })

class FormMeta(Meta):
    def __init__(self, doctype):
        self.doctype = doctype
        self.update_defaults()
        self.load_form()
        self.load_assets()

    def update_defaults(self):
        self.doc = frappe.new_doc(self.doctype)
        self.form = frappe.get_doc("Website Insurance Form", self.doctype).as_dict()


    def load_form(self):
        self.fields = []
        self.steps = []
        self.add_form_fields()
        self.add_steps()

    def add_form_fields(self):
        fields_added = []
        temp = []

        for field in self.form.form_fields:
            if(field.fieldtype == "Section Break"):
                if(len(temp)):
                    self.fields.append(temp)
                temp = []
                continue

            values_to_update = {
                "is_radio": field.is_radio,
                "link_field_filters": field.link_field_filters,
                "description": field.description if field.description else  "",
                "tooltip": field.tooltip if field.tooltip else "",
                "placeholder": field.label,
            }
            if(field.is_table_field and field.table_fieldname not in fields_added):
                self.fields.append(self.add_table_fields(field.table_fieldname))

            elif(not field.is_table_field):
                field_options = self.doc.meta.get_field(field.fieldname).as_dict()

                if(field.fieldtype == "Link"):
                    self.convert_link_field_to_select_field(field, values_to_update)

                field_options.update(values_to_update)

                temp.append(field_options)

            fields_added.append(field.table_fieldname if field.is_table_field else field.fieldname)

        if(len(temp) >= 1):
            self.fields.append(temp)
            temp = []

    def add_table_fields(self, table_fieldname):
        fields = []
        # actual doc tablefield
        table_field_map = get_fields(self.doc.meta.get_field(table_fieldname).options).get("fields_map")

        for field in self.form.form_fields:

            if(field.table_fieldname == table_fieldname and field.fieldname in table_field_map):
                field_options = {
                    "is_radio": field.is_radio,
                    "link_field_filters": field.link_field_filters,
                    "description": field.description if field.description else  "",
                    "tooltip": field.tooltip if field.tooltip else "",
                    "placeholder": field.label,
                }

                if(field.fieldtype == "Link"):
                    self.convert_link_field_to_select_field(field, field_options)

                table_field_map[field.fieldname].update(field_options)
                fields.append(table_field_map[field.fieldname])

        return fields

    def load_table_fields(self):
        self.fields = self.form.form_fields


    def add_steps(self):
        for step in self.form.steps:
            self.steps.append(step)

    def convert_link_field_to_select_field(self, field, field_options):
        options = ""
        if(frappe.db.exists("DocType", field.options)):
            options = "\n".join([ opt.name for opt in frappe.db.sql("""SELECT name
                                        FROM `tab{doctype}`""".format(doctype=field.options), as_dict=True)])

        if(not options):
            options = "Select(%s)"%(field.options)

        field_options.update({
            'fieldtype': 'Select',
            'options': options
        })

    def load_assets(self):
        if self.get('__assets_loaded', False):
            return

        if not self.istable:
            self.add_code()
            self.add_custom_script()

        self.set('__assets_loaded', True)

    def as_dict(self, no_nulls=False):
        d = super(FormMeta, self).as_dict(no_nulls=no_nulls)

        for k in ("__js", "__css", "__list_js", "__calendar_js", "__map_js",
            "__linked_with", "__messages", "__print_formats", "__workflow_docs",
            "__form_grid_templates", "__listview_template", "__tree_js",
            "__dashboard", "__kanban_column_fields", '__templates',
            '__custom_js'):
            d[k] = self.get(k)

		# d['fields'] = d.get('fields', [])

        for i, df in enumerate(d.get("fields") or []):
        	for k in ("search_fields", "is_custom_field", "linked_document_type"):
        		df[k] = self.get("fields")[i].get(k)

        return d

    def add_code(self):
        if self.custom:
        	return

        path = os.path.join(get_module_path(self.form.module), 'forms', scrub(self.doctype))
        def _get_path(fname):
        	return os.path.join(path, scrub(fname))

        system_country = frappe.get_system_settings("country")

        self._add_code(_get_path(self.doctype + '.js'), '__js')
        if system_country:
        	self._add_code(_get_path(os.path.join('regional', system_country + '.js')), '__js')

        self._add_code(_get_path(self.doctype + '.css'), "__css")
        self._add_code(_get_path(self.doctype + '_list.js'), '__list_js')
        if system_country:
        	self._add_code(_get_path(os.path.join('regional', system_country + '_list.js')), '__list_js')

        self._add_code(_get_path(self.doctype + '_calendar.js'), '__calendar_js')
        self._add_code(_get_path(self.doctype + '_tree.js'), '__tree_js')

    def _add_code(self, path, fieldname):
        js = get_js(path)
        if js:
        	self.set(fieldname, (self.get(fieldname) or "")
        		+ "\n\n/* Adding {0} */\n\n".format(path) + js)

    def add_code_via_hook(self, hook, fieldname):
    	for path in get_code_files_via_hooks(hook, self.name):
    		self._add_code(path, fieldname)

    def add_custom_script(self):
        """embed all require files"""
        # custom script
        custom = frappe.db.get_value("Custom Script", {"dt": self.name}, "script") or ""

        self.set("__custom_js", custom)

    def add_linked_document_type(self):
        for df in self.get("fields", {"fieldtype": "Link"}):
        	if df.options:
        		try:
        			df.linked_document_type = frappe.get_meta(df.options).document_type
        		except frappe.DoesNotExistError:
        			# edge case where options="[Select]"
        			pass

    def load_print_formats(self):
        print_formats = frappe.db.sql("""select * FROM `tabPrint Format`
        	WHERE doc_type=%s AND docstatus<2 and disabled=0""", (self.name,), as_dict=1,
        	update={"doctype":"Print Format"})

        self.set("__print_formats", print_formats, as_value=True)

def get_js(path):
    js = frappe.read_file(path)
    if js:
    	return render_include(js)
