/*
    Testing
*/

frappe.provide("personal_auto");

// CAR FORM JS

// progress bar
$('#submit-1 .next').click(function () {
    $('#progress-step-1 .progress-bar').css('width', '100%');
    $('#progress-step-2 .progress-bar').css('width', '10%');
    $('#progress-step-2 button').removeAttr('disabled');
});

$('#submit-2 .next').click(function () {
    $('#progress-step-2 .progress-bar').css('width', '100%');
    $('#progress-step-3 .progress-bar').css('width', '10%');
    $('#progress-step-3 button').removeAttr('disabled');
});

$('#submit-3a .next').click(function () {
    $('#progress-step-3 .progress-bar').css('width', '50%');
});

$('#submit-3b .next').click(function () {
    $('#progress-step-3 .progress-bar').css('width', '100%');
    $('#progress-step-4 .progress-bar').css('width', '10%');
    $('#progress-step-4 button').removeAttr('disabled');
});

$('#progress-step-1 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-1').fadeIn(300);
});

$('#progress-step-2 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-2').fadeIn(300);
});

$('#progress-step-3 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-3-1').fadeIn(300);
});

$('#progress-step-4 button').click(function () {
    $('fieldset').hide();
    $('fieldset#display-4').fadeIn(300);
});

//inside form js
$('input[type=radio][name=car-insurance]').change(function () {
    if (this.value === 'Yes') {
        $('#car-form-1 .check-icons .active').removeClass('d-none');
        $('#car-form-1 .check-icons .inactive').addClass('d-none');
        $('#car-form-2').removeClass('d-none');
        $('#car-form-3').removeClass('d-none');
        $('#car-form-4').addClass('d-none');
        $('#car-form-5').addClass('d-none');
    }
    else if (this.value === 'No') {
        $('#car-form-1 .check-icons .active').removeClass('d-none');
        $('#car-form-1 .check-icons .inactive').addClass('d-none');
        $('#car-form-2').addClass('d-none');
        $('#car-form-3').addClass('d-none');
        $('#car-form-4').removeClass('d-none');
        $('#car-form-5').removeClass('d-none');
    }
});

$('#car-form-3').on('change', 'select', function () {
    $('#form-1-3 .check-icons .active').removeClass('d-none');
    $('#form-1-3 .check-icons .inactive').addClass('d-none');
    $('html, body').animate({
        scrollTop: $('#submit-1').offset().top - 200
    }, 500);
    $('#submit-1 .form-disabled').css('height', '0');
});

$('#car-form-5').on('change', 'select', function () {
    $('#car-form-5 .check-icons .active').removeClass('d-none');
    $('#car-form-5 .check-icons .inactive').addClass('d-none');
    $('html, body').animate({
        scrollTop: $('#submit-1').offset().top - 200
    }, 500);
    $('#submit-1 .form-disabled').css('height', '0');
});

$('.js-vin-form').keyup(function(){
    let count = $(this).val().length,
        icon = $(this).parent().parent().parent().find('.check-icons'),
        nextStep = $(this).parent().parent().parent().next('.js-form-field'),
        nextField = $(this).parent().parent().parent().next().next('.js-form-field-after-hide');
    if (count>0){
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
        $(nextStep).find('.form-disabled').css('height', '100%');
        $(nextField).find('.form-disabled').css('height', '0');
    }
    if (count<=0){
        $(icon).find('.active').addClass('d-none');
        $(icon).find('.inactive').removeClass('d-none');
        $(nextStep).find('.form-disabled').css('height', '0');
        $(nextField).find('.form-disabled').css('height', '100%');
    }
});

$('select.js-select-model').on('change', function () {
    let icon = $(this).parent().parent().parent().prev('.js-form-field').find('.check-icons'),
        nextField = $(this).parent().parent().parent().next('.js-form-field-after-hide');
    $(icon).find('.active').removeClass('d-none');
    $(icon).find('.inactive').addClass('d-none');
    $(nextField).find('.form-disabled').css('height', '0');
});

$('.js-text-form-icon').change(function () {
    let icon = $(this).parent().parent().parent().parent().find('.check-icons');
    if ($(this).val()) {
        $(icon).find('.active').removeClass('d-none');
        $(icon).find('.inactive').addClass('d-none');
    }
    if ($(this).val() === '') {
        $(icon).find('.active').addClass('d-none');
        $(icon).find('.inactive').removeClass('d-none');
    }
});

$('select.display-next-select').on('change', function () {
    let hideSelect = $(this).parent().parent().next('.hide-select');
    $(hideSelect).removeClass('d-none');
});

$('input[name=coverage-begin]').change(function () {
    $('#car-form-26-1 .nice-select-custom').addClass('text-muted').removeClass('select-active');
    $('#car-form-26-1 .nice-select-custom option').prop('selected', function() {
        return this.defaultSelected;
    });
    $('#car-form-26-1 .nice-select-custom').niceSelect('update');
});

$('#car-form-26-1').on('change', 'select', function () {
    $('#car-form-26 .check-icons .active').removeClass('d-none');
    $('#car-form-26 .check-icons .inactive').addClass('d-none');
    $('.js-date-coverage').prop('checked', false);
    $('.js-date-coverage').parent().removeClass('focus active');
    $('#submit-form .form-disabled').css('height', '0');
});

personal_auto.StepForm = Class.extend({

        init: function(args){
            $.extend(this, args);
            this.make();
        },
        make: function(){
            this.$steps_wrapper = $(`<div id="progress-wrap" class="bg-dark py-2">
                                        <div class="container-form container-progress">
                                            <div class="form-row justify-content-center align-items-end text-mute steps-container">
                                        </div>
                                    </div>`).appendTo(".steps-wrapper");

            this.parent = $(".form-wrapper");
            this.make_steps();
        },

        make_steps: function(){

            this.steps = [];
            $.each(frappe.boot.formdata.steps, (idx, step)=>{

                this.add_and_init_step_handler(idx, step);

                let args = $.extend({}, {
                    step: step,
                    fields: frappe.boot.formdata.fields[idx],
                    parent: $(`<div class="h-100" id="${step.section_fieldname}">
                                    <fieldset class="h-100" data-progress="${idx}"></fieldset>
                                </div>`).appendTo(this.parent)
                    });

                this.steps.push(new personal_auto.PersonalAutoController(args))

                if(idx !== 0){
                    this.parent.find(`#${step.section_fieldname}`).hide();
                }
            });
        },

        add_and_init_step_handler: function(idx, step){
            var me  = this;
            let $step = $(`<div class="progress-step-wrapper" class="col-3">
                            <div class="progress-title text-center text-white">
                                <button idx=${idx} section-fieldname="${step.section_fieldname}">${step.label}</button>
                                <i class="fas fa-chevron-down"></i>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-light" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                </div>
                            </div>
                        </div>`).appendTo(this.$steps_wrapper.find(".steps-container"));


            if(idx === 0){
                $step.find(".progress-bar").css("width", "10%");
                $step.attr("is-active", true);
            }

            $step.attr("idx", idx);
            $step.attr("section-fieldname", step.section_fieldname);

            $step.find("button").on("click", function(e){
                me.handle_step_click($step, idx, step);
            });
        },

        handle_step_click: function($cur_step, idx, step){
            let $active_step = this.$steps_wrapper.find(".progress-step-wrapper[is-active='true']");
            if($active_step.attr("idx") == idx){
                return;
            }
            let active_idx = $active_step.attr("idx");
            let active_controller_form = this.steps[active_idx];
            let active_controller_values = active_controller_form.get_values();

            console.log(active_controller_values);
            if(!active_controller_values || active_controller_values){
                $active_step.removeAttr("is-active");
                $active_step.find(".progress-bar").css("width", "0%");

                $cur_step.find(".progress-bar").css("width", "10%");
                $cur_step.attr("is-active", "true");
                this.parent.find(`#${$active_step.attr("section-fieldname")}`).hide();
                this.parent.find(`#${$cur_step.attr("section-fieldname")}`).show();
            }
        }
});


personal_auto.PersonalAutoController =  frappe.ui.BondFieldGroup.extend({
    init: function(args){
        $.extend(this, args);
        this._super(args);
        this.make();
    },
});


new personal_auto.StepForm();
